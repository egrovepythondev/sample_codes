#!/usr/bin/env python
#coding:utf-8
"""
  Author: surfly
  Purpose: Overwrite django send_mail function, https://github.com/surfly/dashboard/issues/603
  Ref implimentation :
  - https://github.com/django/django/blob/master/django/core/mail/backends/smtp.py#L12
  - https://djangosnippets.org/snippets/1864/
  Created: Tuesday 08 September 2015
"""
import base64
import threading
import mandrill

from django.conf import settings
from django.core.mail.backends.base import BaseEmailBackend
from django.core.mail.message import sanitize_address


MANDRIL_API_KEY = settings.MANDRIL_API_KEY


class EmailBackend(BaseEmailBackend):
    def __init__(self, fail_silently=False, **kwargs):
        super(EmailBackend, self).__init__(fail_silently=fail_silently)
        self._lock = threading.RLock()

    def open(self):
        return True

    def close(self):
        pass

    def send_messages(self, email_messages):
        if not email_messages:
            return
        self._lock.acquire()
        try:
            num_sent = 0
            for message in email_messages:
                sent = self._send(message)
                if sent:
                    num_sent += 1
        finally:
            self._lock.release()
        return num_sent

    def _send(self, email_message):
        if not email_message.recipients():
            return False
        from_email = sanitize_address(email_message.from_email, email_message.encoding)
        recipients = [sanitize_address(addr, email_message.encoding)
                      for addr in email_message.recipients()
                      if addr]
        bcc = email_message.bcc[0] if email_message.bcc else""
        message = email_message.body

        try:
            sendmail_by_mandril(email_message.subject, message, message,
                                from_email, recipients,
                                attachments=email_message.attachments,
                                bcc=bcc)
        except:
            if not self.fail_silently:
                raise
            return False
        return True


def sendmail_by_mandril(subject, html_content, text_content,
                        from_email, to, attachments=[], bcc=""):
    mandril_to = []
    for _t in to:
        mandril_to.append({'email': _t,
                           'name': _t.split("@")[0],
                           'type': 'to'})
    if bcc:
        mandril_to.append({'email': bcc,
                           'name': bcc.split("@")[0],
                           'type': 'bcc'})
    try:
        mandrill_client = mandrill.Mandrill(MANDRIL_API_KEY)
        message = {"subject": subject,
                   "from_email": from_email,
                   "from_name": "Surfly",
                   "to": mandril_to,
                   "html": html_content,
                   "text": text_content}
        if attachments:
            message.update({"attachments": [{'content': base64.b64encode(attach[1]),
                                             'name': attach[0].split("/")[-1],
                                             'type': attach[2] or "text/plain"}
                                            for attach in attachments]
                            })
        mandrill_client.messages.send(message=message, async=True)
    except mandrill.Error, e:
        print 'A mandrill error occurred: %s - %s' % (e.__class__, e)
        raise
