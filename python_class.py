class GetCompany_GenerateInvoice(object):
    def __call__(self, today=date.today()):
        self.today = today
        # Will be used for testing calcullation
        self. companys_invoice_details_dict = {}
        self.create()

    def create(self):
        self.yesterday = self.today - timedelta(1)
        self.companys = []
        # get all companys to be create invoice
        self.shortlist_company_list()
        # generate invoice for all companys
        self.generate()
        if TEST_MODE == "0":
            SendReminderMail()

    def generate(self):

        for company in self.companys:
            billing = BillingCalculation()

            billing(company, self.today)
            self. companys_invoice_details_dict.update({
                company.name: {
                    'invoice_id': billing.invoice.id,
                    "now_created": not billing.already_created_by_another_process_or_yesterday
                }
            })

            if billing.already_created_by_another_process_or_yesterday:
                print "invoice created by another process or yesterday"
                continue

            if TEST_MODE == "0":
                GenerateInvoice(billing.invoice)

            del(billing)

    def shortlist_company_list(self):
        invoice_plans = PlanPricing.objects.filter(payment_method="usage_invoice")
        for _plan in invoice_plans:
            # yesterday=date(2016, 2, 28)
            # st = yesterday - relativedelta(months=1)
            # st : date(2016, 1, 28)
            # end= (today - relativedelta(months=1))-timedelta(1)
            # end : date(2016, 1, 31)
            st_date = self.yesterday - relativedelta(months=_plan.payment_period)
            end_day_plus_oneday = self.today - relativedelta(months=_plan.payment_period)

            if end_day_plus_oneday == st_date:
                # case : if today date is 31 and one month plan,
                # Because of timedelta, end date is going to less than start date.
                end_date = end_day_plus_oneday
            else:
                # Need to deduct one day because Django range queryset's end value also included in range
                end_date = end_day_plus_oneday - timedelta(1)
            self.companys.extend(Company.objects.filter(pricing_plan=_plan,
                                                        current_subsciption_startdate__range=[st_date, end_date]))
