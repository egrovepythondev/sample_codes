
import logging
import logging.config

logging.config.fileConfig('logging.conf')
logger = logging.getLogger("reportsApp")

import os
import conf
# import datetime
from dbutils import *
from datetime import datetime, date
from decimal import Decimal
import ho.pisa as pisa
import cStringIO as StringIO
import dropbox
from dropbox_handler import Dropbox_handler


import xlsxwriter
from Cheetah.Template import Template
from reputils import get_password_protected_file, upload_doc, get_downloaded_file_path
from mailutils import send_mail
report_folder_name = conf.STORAGE_DIRECTORY_NAME or 'reports'
xlsx_column_list = [
    '', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N']


def TSGXLSX_REPORT(settings):
    dropbox_email_content = ''
    google_uploaded_list = ['', '']
    report_generated = None
    googledocs_path = None

    db = DBConnect()
    if not(settings['push'] == False and settings['pull'] == True) or not conf.PUSHPULL_REQUIRE:
        update_log = db.update_report_log(
            settings['log_id'], "TSGXLSX Report Started...")

        query = settings['ReportQuery']
        if query:
            file_name = settings['FileName'].split('.')[0] or 'tsgxlsx_report'

            # Get the directory to write the new file.
            dir_path = os.path.normpath(
                datetime.now().strftime(report_folder_name + "/%Y/%B/%d"))

            if not os.path.exists(dir_path):
                os.makedirs(dir_path)

            workbook = xlsxwriter.Workbook(
                '%s/%s.xlsx' % (dir_path, file_name))

            # Save the File Path in ReportLog Table
            file_name = '%s.xlsx' % (file_name)
            file_path = os.path.join(dir_path, file_name)

            update_log = db.update_file_path_report_log(
                settings['log_id'], file_path)

            try:
                msg = "TSGXLSX PAGE Creation Started"
                logger.info(msg)
                update_log = db.update_report_log(settings['log_id'], msg)

                sheet_name = 'Report1'

                tsgxlsx_worksheet(workbook, sheet_name, query)

            except Exception as e:
                print e, 'tsgxlsx_worksheet @ tsgxlsx'

            workbook.close()
            report_generated = True

            msg1 = "TSGXLSX Report Created Successfully..."
            logger.info(msg1)
            update_log = db.update_report_log(settings['log_id'], msg1)

            logger.info("Report will be saved in the path: %s" % file_path)

        else:
            msg = "Please provide the correct format of query"
            logger.info(msg)
            update_log = db.update_report_log(settings['log_id'], msg)

    else:
        file_path = db.get_file_path(settings, settings['ID'])

        if file_path:
            report_generated = True
        else:
            report_generated = False

    if report_generated:
        only_file_name = settings['FileName']
        file_type = 'xlsx'

        if conf.PUSHPULL_REQUIRE:
            obj_dropbox = Dropbox_handler(
                file_path,
                only_file_name,
                file_type)

            if settings['push']:
                dropbox_email_content, google_uploaded_list = upload_doc(
                    obj_dropbox, file_path, settings)
                googledocs_path = google_uploaded_list[1]

            if settings['pull']:
                file_path, dropbox_email_content, googledocs_path = get_downloaded_file_path(
                    settings, obj_dropbox, file_type)

    if settings['SendMail'].lower() == 'yes' or settings['SendMail'].lower() == 'y':
        if settings['password_protect']:
            file_path = get_password_protected_file(file_path, settings)
        send_mail(settings, attach_file=file_path, dropbox_path=dropbox_email_content,
                  googledocs_path=googledocs_path)
    else:
        logger.info("Send Mail configuration is :%s" %
                    (settings['SendMail']))


def TSGXLSXHTML_REPORT(settings):
    db = DBConnect()
    update_log = db.update_report_log(
        settings['log_id'], "TSGXLSXHTML Report Started...")
    query = settings['ReportQuery']
    if query:
            #html_body = tsgxlsxhtml_ebody_generator(settings)
        if settings['SendMail'].lower() == 'yes' or settings['SendMail'].lower() == 'y':
            send_mail(settings)
        else:
            logger.info("SendMail setting is set OFF")
    else:
        msg = "Please provide the correct format of query"
        logger.info(msg)
        update_log = db.update_report_log(settings['log_id'], msg)


def tsgxlsx_worksheet(workbook, worksheet_name, query):
    element_format_dict = {}
    db = DBConnect()
    db_query = query

    worksheet = workbook.add_worksheet(worksheet_name)

    def db_headers_list(db_query):
        table_data = db.dynamic_worksheet_result(db_query)
        table_headers = []
        try:
            db_headers = table_data[0]
            for head, value in db_headers:
                table_headers.append(head)
            return table_headers
        except:
            logger.info("empty data persist ...")
            sys.exit()

    if query.__contains__('|'):
        get_query = query.split('|')
        db_query = get_query[1]

        all_config = get_query[0]
        if all_config.__contains__('+'):
            config_list = all_config.split('+')
        else:
            config_list = [all_config]

        def format_dict_creator(element):
            if element_format_dict.has_key(element):
                pass
            else:
                element_format_dict[element] = {}

        def format_updater(element, new_format):
            for item in element:
                format_dict_creator(item)
                element_format_dict[item].update(new_format)

        def format_checker(style, element):

            if style.__contains__(':'):
                try:
                    style_name = style.split(':')
                    style = style_name[0].lower()
                    font_color = style_name[1].lower()

                    if style == "color":
                        format_updater(element, {'font_color': font_color})
                    elif style == "bgcolor":
                        format_updater(element, {'bg_color': font_color})

                    alignment = font_color  # changing name to alignment
                    if alignment.lower().__contains__("left"):
                        format_updater(element, {'text_h_align': 1})

                    if alignment.lower().__contains__("center"):
                        format_updater(element, {'text_h_align': 2})

                    if alignment.lower().__contains__("centre"):
                        format_updater(element, {'text_h_align': 2})

                    if alignment.lower().__contains__("right"):
                        format_updater(element, {'text_h_align': 3})

                except:
                    logger.info("please provide correct color format ...")

            if style.lower().__contains__("bold"):
                format_updater(element, {'bold': True})

            if style.lower().__contains__("italic"):
                format_updater(element, {'italic': True})

            if style.lower().__contains__("underline"):
                format_updater(element, {'underline': True})

            if style.lower().__contains__("grid"):
                format_updater(element, {'border': 1})

        for config in config_list:
            temp_config = config.split('-')
            style = temp_config[0]
            elements_list = temp_config[1]
            element = elements_list.split(',')
            format_checker(style, element)

    data_list = db.get_result(db_query)
    data_headers = [db_headers_list(db_query)]
    for item in data_list:
        data_headers.append(item)

    def set_cell_format(table_head, _date=False):

        if element_format_dict.has_key(table_head):
            cell_format = workbook.add_format(element_format_dict[table_head])
        else:
            cell_format = workbook.add_format()

        return cell_format

    def write_cell_data(row, col, cell_data):
        cell_format = cell_data[0]
        cell_data = cell_data[1]

        if not (isinstance(cell_data, date) or isinstance(cell_data, Decimal)):
            cell_format = set_cell_format(cell_format)

        elif isinstance(cell_data, date):
            _cell_format = cell_format
            cell_format = set_cell_format(cell_format)
            cell_format.set_num_format('yyyy-mm-dd')

            if len(_cell_format) < 12:
                worksheet.set_column(col, col, 12)
            cell_data = date.strftime(cell_data, '%Y-%m-%d')
            cell_data = datetime.strptime(cell_data, "%Y-%m-%d")

        elif isinstance(cell_data, Decimal):
            cell_format = set_cell_format(cell_format)
            cell_format.set_num_format('$0.00')

        worksheet.write(row, col, cell_data, cell_format)

    column_width_spec = {}

    for row, row_data in enumerate(data_headers):

        for col, cell_data in enumerate(row_data):
            if row == 0:
                cell_data = ('Head_' + cell_data, cell_data)

            if isinstance(cell_data[1], basestring) or isinstance(cell_data[1], unicode):

                if not column_width_spec.has_key(col):
                    column_width_spec.update({col: len(cell_data[1]) + 1})
                    worksheet.set_column(col, col, column_width_spec[col])

                if column_width_spec[col] < len(cell_data[1]):
                    column_width_spec.update({col: len(cell_data[1]) + 1})
                    worksheet.set_column(col, col, column_width_spec[col])

            elif isinstance(cell_data[1], Decimal):

                if not column_width_spec.has_key(col):
                    column_width_spec.update(
                        {col: len(cell_data[1].__str__()) + 1})
                    worksheet.set_column(col, col, column_width_spec[col])

                if column_width_spec[col] < len(cell_data[1].__str__()):
                    column_width_spec.update(
                        {col: len(cell_data[1].__str__()) + 1})
                    worksheet.set_column(col, col, column_width_spec[col])

            write_cell_data(row, col, cell_data)


def tsgxlsxhtml_ebody_generator(settings):
    element_format_dict = {}
    db = DBConnect()
    db_query = query = settings['ReportQuery']

    def db_headers_list(db_query):
        table_data = db.dynamic_worksheet_result(db_query)
        table_headers = []
        try:
            db_headers = table_data[0]
            for head, value in db_headers:
                table_headers.append(head)
            return table_headers
        except:
            logger.info("empty data persist ...")
            sys.exit()

    if query.__contains__('|'):
        get_query = query.split('|')
        db_query = get_query[1]

        all_config = get_query[0]
        if all_config.__contains__('+'):
            config_list = all_config.split('+')
        else:
            config_list = [all_config]

        def format_dict_creator(element):
            if element_format_dict.has_key(element):
                pass
            else:
                element_format_dict[element] = {}

        def format_updater(element, new_format):
            for item in element:
                format_dict_creator(item)
                element_format_dict[item].update(new_format)

        def format_checker(style, element):

            if style.__contains__(':'):
                try:
                    style_name = style.split(':')
                    style = style_name[0].lower()
                    font_color = style_name[1].lower()

                    if style == "color":
                        format_updater(element, {'color': str(font_color)})
                    elif style == "bgcolor":
                        format_updater(
                            element, {'background-color': str(font_color)})

                    # changing it according to alignment
                    alignment = font_color
                    if alignment.lower().__contains__("left"):
                        format_updater(element, {'text-align': 'left'})

                    if alignment.lower().__contains__("right"):
                        format_updater(element, {'text-align': 'right'})

                    if alignment.lower().__contains__("center"):
                        format_updater(element, {'text-align': 'center'})

                except:
                    logger.info("please provide correct color format ...")

            if style.lower().__contains__("bold"):
                format_updater(element, {'font-weight': 'bold'})

            if style.lower().__contains__("italic"):
                format_updater(element, {'font-style': 'italic'})

            if style.lower().__contains__("underline"):
                format_updater(element, {'text-decoration': 'underline'})

            if style.lower().__contains__("grid"):
                format_updater(
                    element, {'border': '2px solid black !important'})

        for config in config_list:
            temp_config = config.split('-')
            style = temp_config[0]
            elements_list = temp_config[1]
            element = elements_list.split(',')
            format_checker(style, element)

    data_list = db.get_result(db_query)
    modified_data_list = []
    for row_data in data_list:
        modified_row_list = []
        for item in row_data:
            if isinstance(item[1], Decimal):
                modified_row_list.append((item[0], '$' + item[1].__str__()))
            else:
                modified_row_list.append(item)
        modified_data_list.append(modified_row_list)

    data_headers = db_headers_list(db_query)
    table_headers = []
    for header in data_headers:
        table_headers.append(("Head_" + str(header), str(header)))

    style_script = ""
    for style_name, style_value in element_format_dict.iteritems():
        style_script += ".{0}{1}".format(style_name, style_value)

    style_script = style_script.replace(',', ";").replace("'", "")

    if settings['EmailBody']:
        msg = settings['EmailBody']

    template = Template(
        file="html/tsgxlsxhtml_template.html",
        searchList=[
            {
                'msg': msg,
                'data_list': modified_data_list,
                'table_headers': table_headers,
                'style_script': style_script
            }
        ])
    return str(template)
