/*
-This JS Contains following functionalities.(Here Widget refers chatbox)

1.Initialization
i.Common Initialization
ii.Enable scroll with in text chat
2.Session URL related functions
i.Copy event
ii.Send mail event
3.Header Functionalities
i.Dock
ii.Adduser
iii.Switch Control
iv.Navigate to new url
v.session end avgrund
vi.grey line functionality
4.Video chat
5.Text chat
6.Drag
7.Tool tip
8.Resize
9.Indicating user to update his/her IE browser
10.Video full screen mode
11.iframe size settings
12.common functionalities
 */

//To check video and audio chat change
var is_video = false;
var is_text = false;
var mod = angular.module("surflyModule", ["ngClipboard", "ui.avgrund", "ngSanitize"]);
/*
 * To change the angular bind symbol {{}} to [[]] - Reason:both angularJS and
 * django uses the same symbol for binding
 */
mod.config(function($interpolateProvider, ngClipProvider) {
    $interpolateProvider.startSymbol('[[');
    $interpolateProvider.endSymbol(']]');
    ngClipProvider.setPath("/static/libs/zeroclipboard/ZeroClipboard.swf");
});

function maximize_streams(spectator_mode) {
    // Append remote streams
    $("#remote_streams").appendTo("#full_screen_video .streams");

    // Append local stream
    if (scope.is_local_stream && !spectator_mode && !voxAPI.useFlashOnly) {
        var local_stream_container = $("<div>");
        local_stream_container.addClass("vid user_streams local_full_screen_stream");
        local_stream_container.css("float", "left");
        local_stream.appendTo(local_stream_container);
        if (owner_name) {
            name_div = $("<div>");
            name_div.addClass("name");
            name_div.text(owner_name);
            name_div.appendTo(local_stream_container);
        }
        local_stream_container.appendTo("#remote_streams");
        local_stream.show();
    }

    $( "#full_screen_video .streams video" ).each(function(){
        $(this)[0].play();
    });
}

function minimize_streams(spectator_mode) {
    if (scope.is_local_stream && !spectator_mode && !voxAPI.useFlashOnly) {
        $("#local_stream .streams").append(local_stream);
        local_stream[0].play();
    }
    $(".local_full_screen_stream").remove();
    $("#full_screen_video .streams #remote_streams").prependTo("#video_chat");
    $("#remote_streams video").each(function(){
            $(this)[0].play();
    });
}

/* Widget Controller */
mod.controller("SessionController", function($scope, $timeout, GetHeightService, $element, $window, $interval, $http) {
    // ---- 1.Initialization - starts here ----//
    /** 1 - i.Common Initialization * */
    //To remove the blured effect on mac safari
    $scope.is_loaded = true;
    $scope.user_origin = 0;
    //To store id's of video_stream enabled user's
    $scope.video_enabled_uid = [];
    $scope.video_status = [];
    // Tooltip text for video and audio icons
    $scope.vicon_tooltip_text = "Start videochat";
    $scope.aicon_tooltip_text_microphone = "Mute";
    $scope.voximplant_dial_number = voximplant_dial_number;
    $scope.voximplant_pin = voximplant_pin;
    $scope.is_conf_initiator = false;

    // To set width of footer text-content
    // To store custom_width and custom_height
    $scope.custom_width = $(window).width();
    $scope.custom_height = $(window).height();
    $scope.custom_orig_width = $(window).width();
    $scope.custom_orig_height = $(window).height();
    // To store width and height of leader and all followers
    $scope.window_width_list = {};
    $scope.window_height_list = {};
    // To disable video icon for followers when webrtc not enabled
    $scope.is_vicon_disabled = false;
    // To enable avgrund for add a user
    $scope.modalIsVisible = false;
    // To enable avgrund for switch control
    $scope.isSwitchControl = false;
    // To enable avgrund for navigate to new url
    $scope.isNaviagate2New = false;
    // To enable avgrund for session end
    $scope.sessionEndFollwers = false;
    // To store bistri id of user
    $scope.my_conf_id = 0;
    // To check dock mode
    $scope.is_dock = false;
    // To show/hide full screen video
    $scope.full_screen_display = false;
    $scope.is_spectator_mode = false;
    // To hide -"Share the following url:" text from the session
    // start page.
    $scope.session_content_hide = false;
    // To store the list of all users for switch control
    $scope.user_list = [];
    // To store user messages
    $scope.user_messages = [];
    // To store the user count to display in the header
    $scope.total_user_count = 1;
    // To hide the overlay div- to enable the resize over iframe
    $scope.is_overlay_hide = true;
    // To store browser version number
    $scope.msie = 0;
    // To get height of the header when docked
    $scope.chat_aft_dock_height = $("#chat_aft_dock").height();
    // To display the widget in the bottom of the iframe
    $scope.box_bottom = '30px';
    $scope.box_right = "50px";
    $scope.box_height = "275px";
    $scope.temp_wid_height = parseInt($scope.box_height);
    $scope.box_width = "250px";
    $scope.copy_class = "copyed-button";
    $scope.copy_btn_text="Copy";
    $scope.copy_btn_icon="fa fa-copy";
    $scope.email_class = "email-button";
    $scope.is_muted = false;
    $scope.box_right_ref = (($(window).width() - $scope.custom_width) / 2);
    $scope.box_top_ref = ($scope.custom_height / 2) - parseInt($scope.chat_aft_dock_height) / 2;
    $scope.msg_focus = false;
    $scope.ui_off = ui_off;
    //White Label controls
    $scope.white_label = white_label;
    $scope.white_label_position = "30px";
    //To check local stream - created or not
    $scope.is_local_stream = false;
    //To display/hide small video div(ie,container of user's own stream)
    $scope.is_local_hide = true;
    // To enable bootbox to leader while follower request control
    $scope.isRequestControl = false;
    // To set the height of the widget content- other than
    // header and footer
    $scope.dialog_content_style = {
        'height': (GetHeightService.CalculateHeight() + "%")
    };
    $scope.ng_mic_control = 'fa fa-microphone-slash';
    $scope.ng_video_control = 'fa fa-video-camera';
    $scope.dialog_session_start_style = {
        'height': "100%"
    };
    $scope.takecontrol = true;
    $scope.backcontrol = false;
    $scope.have_control = is_owner ? true : false;
    if(!docked_only) {
        $scope.hide_chatbox = false;
    }
    else if(docked_only) {
        $scope.is_dock = true;
    }

    if(videochat) {
        $scope.vicon_disable = {"pointer-events": "none"};
        $("#init_video_chat").css("opacity", "0.5");
    }
    $scope.enable_hidebox_control = false;
    // To set user id
    $scope.userTyping = false;
    $scope.isTyping = 0;

    $scope.isEndSession = false;
    $scope.follower_id = false;
    $scope.follower_choice = false;

    $scope.chat_box_color = chat_box_color;

    $scope.file_progress = function(ref, progress, emsg, pdf, doc){
        angular.forEach($scope.user_messages, function(v,k){
            if(v.ref == ref)
            {
                v.progress = progress;
                v.emsg = emsg;
                v.pdf = pdf;
                v.doc = doc;

                /*
                    1. tooltip for download/view document
                    2. remove progress bar when complete/failed
                */
                $timeout(function(){
                    if(progress === 100)
                    {
                        $(".user-comments[data-ref='"+ref+"'] a[rel='tooltip']").tooltip({
                                selector: '',
                                placement: 'top',
                                container: 'body'
                        });

                        $(".user-comments[data-ref='"+ref+"'] .progress_complete").slideUp("3000");
                    }
                    else if(emsg !== undefined && emsg != '')
                    {
                        $(".user-comments[data-ref='"+ref+"'] .failed_upload .progress").slideUp("3000");
                    }
                });
            }
        });
    };

    /* ******Log Chat history starts here********** */

    $scope.log_message = '';
    $scope.is_text_chat_start = false;
    $scope.new_log_message = function(msg)
    {
        dt_now = new Date();

        doc = msg.doc !== undefined ? "Shared a document : " : "";

        if(msg.class == "video-support" || msg.class == "session-cont")
            $scope.log_message += "["+ dt_now.toTimeString() +"] "+msg.id+msg.msg+"\n";
        else if(msg.id == "me:")
            $scope.log_message += "["+ dt_now.toTimeString() +"] <"+owner_name+"> "+doc+msg.msg+"\n";
        else
            $scope.log_message += "["+ dt_now.toTimeString() +"] <"+msg.id.split(":")[0]+"> "+doc+msg.msg+"\n";

        if(!$scope.is_text_chat_start && msg.class != "video-support" && msg.class != "session-cont")
            $scope.is_text_chat_start = true;
    };

    $scope.send_log_data = function(){
        if(!$scope.is_text_chat_start || !$scope.log_message)
            return false;

        $http.post("/save/chat/history/", {log: $scope.log_message, session_id: session_id})
            .success(function(response){
                $scope.log_message = '';
            });
    };

    if(is_owner && store_chat_logs){
        $interval($scope.send_log_data, 10000);
    }

    /* ******Log Chat history ends here********** */
    // ---- 4 -> Ends Here ----//

    // To store messages in user_message array
    $scope.storeMsg = function(cls, id, msg, other_user, me_user, doc, pdf, ref, progress, emsg) {

        msgobj = {"class": cls, "id": id, "msg": (msg !== undefined ? unescape(msg) : ''), "read":1};

        if(other_user != undefined || me_user != undefined) {
            msgobj.other_user = other_user;
            msgobj.me_user = me_user;
            msgobj.read = id !== 'me:' ? 0 : 1;

            if(msgobj.read == 0 && enable_sounds)
                $('#received_notifications').trigger('play');
        }

        if(doc !== undefined)
        {
            msgobj.doc = doc;
            msgobj.pdf = pdf;
            msgobj.ref = ref;
            msgobj.progress = progress;
            msgobj.emsg = emsg;
        }

        $scope.new_log_message(msgobj);
        $scope.user_messages.push(msgobj);
        $scope.unread_message();
    }

    $scope.unread_message = function(msg){

        if(msg !== undefined)
        {
            if(msg.read)
                return;

            msg.read = 1;
        }

        var cnt = 0;
        angular.forEach($scope.user_messages, function(v,k){
            if(v.read == 0)
                cnt++;
        });
        $scope.title_notification(cnt);
        return cnt;
    };

    $scope.title_notification = function(cnt){
        page_title = $("title").text();
        if(page_title.indexOf(") ") != -1)
            page_title = page_title.split(") ")[1];

        $("title").text(cnt ? "("+cnt+") "+page_title : page_title);
    };

    $scope.reset_unread_message = function(msg){
        angular.forEach($scope.user_messages, function(v,k){
            v.read = 1;
        });
        $scope.title_notification(0);
    };

    $scope.focus = function () {
        $scope.reset_unread_message();
        $scope.msg_focus = true;
        // show the chatbox
        if (is_video) {
            $scope.Disp_text_w_video();
        }
    }

    $scope.storeMsg("session-cont", "Your session has started.", "");
    /** 1 - i -> ends here * */

    /** 1 - ii.Enable scroll with in text chat starts here * */
    if (!docked_only) {
        var time_out_scroll;
        // Disable drag when scrolling the textchat window
        $("#text_chat").on("scroll", function() {
            $("#widget").draggable('disable');
            if (time_out_scroll != null) {
                $timeout.cancel(time_out_scroll);
            }
            time_out_scroll = $timeout($scope.EnableDrag, 500);
        });

        // Enable drag after scroll in textchat window
        $scope.EnableDrag = function() {
            $("#widget").draggable('enable')
        }
    }

    /** 1 - ii ends here * */

    // ---- 1 ends here ----//
    // ---- 2.Session URL related functions -starts here ----//
    // To take reference of the current page before copy or send mail
    $scope.time_out_var;
    // To enable iframe
    $scope.session_ref = false;

    /** 2 -> i.Copy event - starts here * */
    // Session Url copy event by using 'ZeroClipboard' library
    $scope.showMessage = function() {
        $scope.copy_class = "copy-button";
        $scope.copy_btn_text="Copied";
        $scope.copy_btn_icon="fa fa-check";
        $scope.chat_box_copy_dis = true;
        $scope.SessionActivation();
    };
    /** 2 -> i ends here* */

    /** 2 -> ii.Send mail event * */
    // Email button click to navigate to invite by email page
    $scope.chat_box_email = function() {
        // Invite email page id
        $scope.page = "dialog_invite_email";
        $scope.dialog_invite_email_style = {
            'height': (GetHeightService.CalculateHeight() + "%")
        };
    }

    // Cancel button click in invite by email window to navigate
    // to old
    // window-session start window
    $scope.send_invite_cancel = function() {
        $.noty.closeAll();
        // Session Start(Ist Page) Id
        $scope.page = "dialog_session_start";
        $scope.input_link = localStorage.getItem("inputLink");
        $scope.dialog_content_style = {
            'height': (GetHeightService.CalculateHeight() + "%")
        };
    }

    // Send invite button click in email window
    $scope.send_invite = function() {
        var email_regex = /^((\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)\s*[,]{0,1}\s*)+$/;
        var their_email_val = email_regex
            .test($("#their_email").val());
        var your_email_val = email_regex.test($("#your_email")
            .val());
        if (their_email_val && your_email_val) {
            $.noty.closeAll();
            // Invitation send - success page Id
            $scope.page = "dialog_send_invite";
            $scope.dialog_send_invite_style = {
                'height': (GetHeightService.CalculateHeight() + "%")
            };
            // Send Invitation
            $scope.InviteSendAjaxCall();
            TrackSurflyEvents('Invitation by email', {email:$("#their_email").val()});
        } else {
            notify_to = (!their_email_val) ? "'their email'" : "'your email'";

            noty({
                text: "Enter valid email id in "+ notify_to +" field.",
                type: "error",
                timeout: "15000"
            });
        }
    }

    // Send invitation through ajax call
    $scope.InviteSendAjaxCall = function() {
        var form = $("#invite_session_user");
        var error_msg = $("#error-message");

        // Call the email function for the session user
        $.ajax({
            url: "/share/session-url/",
            type: "POST",
            data: form.serialize(),
            success: function(data) {
                if (data.result == "success") {
                    $scope.SessionActivation();
                }
                error_msg.html(data.message);
            },
            error: function(xhr, textStatus, error) {
                error_msg.html(error);
            }
        });
    }

    /** 2 -> ii ends here * */

    /** Session URL - common functions * */
    // Time out function ---> after copy and send invite action
    $scope.SessionActivation = function() {
        $scope.timerRef = $timeout($scope.SessionActivated, 3000);
    }

    // session activation
    $scope.SessionActivated = function() {
            $timeout.cancel($scope.timerRef);
            $scope.session_ref = true;
            $("#user_chat_div").removeClass("bg-opcity");
            $scope.dialog_videowtext_chat_window_style = {
                'height': "100%"
            };
            $scope.dialog_content_style = {
                'height': (GetHeightService.CalculateHeight() + "%")
            };
            $scope.hideModal();

        }
        /** Session Activation - common functions ends here * */

    // ---- 2 -> ends here ----//
    // ---- 3.Header Functionalities - starts here ----//
    /** 3 - i.Dock starts here * */
    // Dock functionality
    $scope.chatBoxDock = function() {
        // Remove resize and drag functionality
        if (!docked_only) {
            $("#widget").resizable("disable").draggable("disable");
        }
        if (navigator.appName == "Microsoft Internet Explorer" && msie >= 9 && !$scope.session_ref) {} else {
            $scope.is_dock = true;
            var myEl = angular.element(document.querySelector('#widget'));
            if(dock_top_position) {
                $scope.widget_class = true;
                $scope.box_left = "";
                $scope.box_top = "";
                $scope.box_right_ref = (($(window).width() / 2) + ($(window).height() / 2)) / 2;
                $scope.box_right_ref = ($scope.box_right_ref >= 0) ? $scope.box_right_ref : 0;
                $scope.box_bottom_ref = $(window).scrollTop() + $(window).height();
                $scope.box_right = $scope.box_right_ref + "px";
                $scope.box_mh = $scope.box_height = "0px";
                $scope.temp_wid_height = parseInt($scope.box_height);
                $scope.box_bottom = $scope.box_bottom_ref + 'px';
                $scope.box_width = "0px";
                $scope.box_cursor = "auto";
                $scope.is_local_hide = true;
                myEl.css({
                    'left': "",
                    'top': "",
                    'right': $scope.box_right,
                    'bottom': $scope.box_bottom
                });
            } else {
                $scope.widget_class = true;
                $scope.box_right = "";
                $scope.box_bottom = "";
                $scope.box_left_ref = (($(window).width() - $scope.custom_width) / 2);
                $scope.box_left_ref = ($scope.box_left_ref >= 0) ? $scope.box_left_ref : 0;
                $scope.box_top_ref = ($scope.custom_height / 2) - parseInt($scope.chat_aft_dock_height) / 2;
                $scope.box_left = $scope.box_left_ref + "px";
                $scope.box_mh = $scope.box_height = "0px";
                $scope.temp_wid_height = parseInt($scope.box_height);
                $scope.box_top = $scope.box_top_ref + 'px';
                $scope.box_width = "0px";
                $scope.box_cursor = "auto";
                $scope.is_local_hide = true;
                myEl.css({
                    'right': "",
                    'bottom': "",
                    'left': $scope.box_left,
                    'top': $scope.box_top
                });
            }
        }
    }

    // Expand functionality
    $scope.chatBoxUndock = function() {
        $("#widget").removeAttr("style");

        $scope.widget_class = false;
        if (!$scope.is_muted && $scope.is_local_stream)
            $scope.is_local_hide = false;
        $scope.box_top = "";
        $scope.box_bottom = ($(window).height() - $scope.custom_height) + "px";
        $scope.box_right = (($(window).width() - $scope.custom_width) / 2) + (($scope.custom_width * 10) / 100) + "px";
        $scope.box_left = "";
        $scope.box_cursor = "move";
        $scope.box_mh = "275px";
        $scope.box_height = "275px";
        $scope.temp_wid_height = parseInt($scope.box_height);
        $scope.box_width = "250px";
        $scope.dialog_content_style = {
            'height': (100 - ((95 / 275) * 100)) + "%"
        };
        $scope.dialog_videowtext_chat_window_style = {
            'height': "100%"
        };
        $scope.is_dock = false;
        $("#local_stream").removeAttr("style")
        var is_userstream = $("#user-stream").val();
        if ($scope.msg_focus && is_userstream){
            $scope.BlockChatBox();
        }
        if (!docked_only) {
            $("#widget").resizable("enable").draggable("enable");
        }
    }

    /** 3 -> i ends here * */

    /** 3 - ii.AddUser starts here * */
    // Add user functionality in normal mode
    // To enable and disable the Agrund
    $scope.hideModal = function() {
        $scope.modalIsVisible = false;
        if(!docked_only) {
            $scope.hide_chatbox = false;
        }
        else {
            $scope.is_dock = true;
        }
    };

    $scope.showModal = function() {
        if ($scope.isSwitchControl)
            $scope.hideSwitchControl();
        if ($scope.isNaviagate2New)
            $scope.hideNaviagate2New();
        if ($scope.sessionEndFollwers)
            $scope.hidesessionEndFollwers();
        $scope.modalIsVisible = true;
        if(!docked_only) {
            $scope.hide_chatbox = true;
        }
        else {
            $scope.is_dock = false;
        }
    };

    $scope.addUser = function() {
        if (navigator.appName == "Microsoft Internet Explorer" && msie >= 9 && !$scope.session_ref) {} else {
            $scope.page = "dialog_session_start";
            $scope.session_content_hide = true;
            $scope.input_link = localStorage
                .getItem("inputLink");
            $scope.copy_class = "copyed-button";
            $scope.copy_btn_text="Copy";
            $scope.copy_btn_icon="fa fa-copy";
            $scope.chat_box_copy_dis = false;
            $scope.showModal();
            TrackSurflyEvents('Clicked add user button');
        }
    }

    /** 3 -> ii ends here * */

    /** 3 -> iii.Switch Control starts here * */
    // To enable and disable the Agrund
    $scope.hideSwitchControl = function() {
        $scope.isSwitchControl = false;
        if(!docked_only) {
            $scope.hide_chatbox = false;
        }
        else {
            $scope.is_dock = true;
        }
    };

    $scope.showSwitchControl = function() {
        if ($scope.modalIsVisible)
            $scope.hideModal();
        if ($scope.isNaviagate2New)
            $scope.hideNaviagate2New();
        if ($scope.sessionEndFollwers)
            $scope.hidesessionEndFollwers();
        $scope.isSwitchControl = true;
        if(!docked_only) {
            $scope.hide_chatbox = true;
        }
        else {
            $scope.is_dock = false;
        }
    };

    $scope.switch_control = function() {
        $scope.showSwitchControl();
    }

    $scope.request_control = function() {
        broadcastMsg('REQUEST_CONTROL');
        $scope.storeMsg("session-cont", "Your Request has been sent.");
        noty({
            text: "Your Request has been sent",
            type: "success",
            timeout: "15000"
        });
        $timeout(function() {
            $scope.switch_disable = {"pointer-events": "none"};
        }, 1000);
    }
    $scope.control_back = function() {
        broadcastMsg('REQUEST_BACK');
        $scope.takecontrol = true;
        $scope.backcontrol = false;
        $timeout(function() {
            $scope.switch_disable = {"pointer-events": "block"};
        }, 1000);
    }
    $scope.grantRequestControl = function() {
        $scope.isRequestControl = false;
        broadcastMsg('REQUEST_GRANT');
    };
    $scope.hideRequestControl = function() {
        $scope.isRequestControl = false;
        broadcastMsg('REQUEST_CANCEL');
    };
    /** 3 -> iii ends here * */

    /** 3 -> iv.Navigate to new url starts here * */
    // To enable and disable the Agrund
    $scope.hideNaviagate2New = function() {
        $scope.isNaviagate2New = false;
        if(!docked_only) {
            $scope.hide_chatbox = false;
        }
        else {
            $scope.is_dock = true;
        }
    };



    //Show avgrund function for Navigate to new url
    $scope.showNaviagate2New = function() {
        if ($scope.modalIsVisible)
            $scope.hideModal();
        if ($scope.isSwitchControl)
            $scope.hideSwitchControl();
        $scope.isNaviagate2New = true;
        if(!docked_only) {
            $scope.hide_chatbox = true;
        }
        else {
            $scope.is_dock = false;
        }
    };

    $scope.Navigate = function(docurl, docname) {
        url = (docurl === undefined) ? $('#inputUrl').val() : docurl;
        url = validate_url(url);
        if (url) {
            switch_to(menu_counter);
            sendToCobro(
                'tab_command',
                {cmd: 'relocate_tab', url: url}
            );

            if(docurl === undefined)
            {
                $scope.storeMsg("session-cont", "Opening", url);
                broadcastMsg("NAVIGATE_NEW_URL:::" + url);
                TrackSurflyEvents('Clicked new-url', {url:url});
            }
            else
            {
                $scope.storeMsg("session-cont", "Viewing", '"'+docname+'"');
                broadcastMsg("NAVIGATE_NEW_URL:::" + docurl+":::"+docname);
            }
        }
        $scope.hideNaviagate2New();
    }

    /** 3 -> iv ends here * */

    /** 3 ->v.session end avgrund **/
    //hide function for session end avrgrund
    $scope.hidesessionEndFollwers = function() {
        $scope.sessionEndFollwers = false;
        if(!docked_only) {
            $scope.hide_chatbox = false;
        }
        else{
            $scope.is_dock = true;
        }
    };

    //Show avgrund function for session end
    $scope.showEndSession = function() {
        if ($scope.modalIsVisible)
            $scope.hideModal();
        if ($scope.isSwitchControl)
            $scope.hideSwitchControl();
        $scope.sessionEndFollwers = true;
        if(!docked_only) {
            $scope.hide_chatbox = true;
        }
        else {
            $scope.is_dock = false;
        }
    };
    /** 3-> v ends here **/

    /** 3 -> vi ends here **/
    // ---- 3 -> Ends here ----//

    // ---- 4.Video Chat - starts here ----//
    // Video chat enable and disable action
    if (!docked_only) {
        $scope.init_audio_class = "footer-icon-6";
        $scope.tc_class = "content-bar chat-window heig-cont";
        $scope.vc_class = "img-cont";
        $scope.tc_scroll_class = "scrol-bar";
        $scope.vc_tc_class = "content-bar video-chat";
        $scope.audio_notification = true;
        $scope.init_video_chat = function() {
            if (navigator.appName == "Microsoft Internet Explorer" && msie >= 9 && !$scope.session_ref) {} else {
                is_text = false;
                var l = $("#remote_streams .user_streams").length;
                if (l == 0) {
                    is_video = false;
                    $timeout.cancel($scope.time_out_var);
                    $scope.vc_tc_class = "content-bar video-chat";
                    $scope.vc_tc_height = "100%";
                    $scope.tc_height = "100%";
                    $scope.tc_class = "content-bar chat-window heig-cont";
                    $scope.is_tc_hide = false;
                    $scope.is_vc_hide = true;
                    $scope.is_cancel_hide = true;
                    $scope.tc_scroll_class = "scrol-bar";

                    $scope.dialog_content_style = {
                        'height': (GetHeightService
                            .CalculateHeight() + "%")
                    };
                } else {
                    is_video = true;
                    $scope.ToggleClose();
                    $scope.is_vc_hide = false;
                    $scope.is_tc_hide = true;
                    $scope.vc_class = "content-img";
                    $scope.vc_height = "100%";
                    $scope.tc_class = "";
                    $scope.vc_tc_class = "";
                    $scope.vc_tc_height = "100%";
                    $scope.dialog_content_style = {
                        'height': (GetHeightService
                            .CalculateHeight() + "%")
                    };
                    $scope.tc_scroll_class = "";
                    $("#videochat").addClass("full_screen").removeClass("small_screen");
                }
            }
        }
    }
    // ---- 4 -> Ends Here ----//

    // ---- 5.Textchat - starts here ----//
    if (!docked_only) {
        $scope.is_tc_hide = false;
        $scope.is_vc_hide = true;
        $scope.is_cancel_hide = true;
        $scope.user_chat_msg = function(keycode, event) {
            $scope.reset_unread_message();
            if(event == 'blur') {
                $scope.userTyping = false;
                broadcastMsg("userstoptyping");
            }
            else if (keycode == 13) {
                if($scope.chat_msg != undefined && $scope.chat_msg != 'undefined' && $scope.chat_msg !='') {
                    $scope.userTyping = false;
                    broadcastMsg("userstoptyping");
                    $scope.storeMsg('chat_0' + $scope.my_index, "me:",
                        $scope.chat_msg, other_user, me_user);
                    if (enable_sounds) {
                        $('#send_notifications').trigger('play');
                    }
                    // Broadcasting message to all
                    if (owner_name==""){
                        broadcastMsg($scope.chat_msg + "*" + other_user + "*" + me_user);
                    }
                    else{
                        broadcastMsg($scope.chat_msg + "*" + owner_name + "*" + other_user + "*" + me_user);
                    }
                    $scope.chat_msg = "";
                    if (is_video) {
                        $scope.Disp_text_w_video();
                    } else {
                        if ($scope.time_out_var != null)
                            $timeout.cancel($scope.time_out_var);
                        $scope.tc_height = "100%";
                        $scope.is_tc_hide = false;
                    }
                }
            }
            else {
                $scope.Disp_text_w_video();
                if(!$scope.userTyping) {
                    $scope.userTyping = true;
                    broadcastMsg("userstarttyping"+"*"+owner_name);
                }
                if ($scope.isTyping != 0) {
                    $timeout.cancel($scope.isTyping);
                }
                $scope.isTyping = $timeout(function() {
                    $scope.userTyping = false;
                    broadcastMsg("userstoptyping");
                    if(is_video) {
                        $timeout($scope.BlockChatBox,30000);
                    }
                }, 5000);
            }
        };

        // Animation for text chat when video enabled
        $scope.Disp_text_w_video = function(scroll) {

            if (!is_video) {
                return false;
            }

            if ($scope.time_out_var != null)
                $timeout.cancel($scope.time_out_var);
            $scope.vc_tc_class = "content-bar chat-window scr-cht";
            $scope.tc_class = "session-content-1 chat-window";
            $scope.tc_scroll_class = "scrol-bar scroll-bar-2";
            $scope.vc_class = "img-cont";
            if(!$scope.msg_focus) {
                $scope.time_out_var = $timeout($scope.BlockChatBox,30000);
            }
            $("#remote_streams").click(function(){
                $scope.BlockChatBox();
            });
            if (!is_text) {
                is_text = true;
                $scope.vc_tc_height = "100%";
                $scope.close_height = "16px";
                $scope.tc_position = "absolute";
                $scope.tc_bottom = "50px";
                $scope.is_tc_hide = true;

                var is_userstream = $("#user-stream").val();

                $scope.vc_height = "0%";
                $("#video_chat").css({'height': $scope.vc_height}).addClass("small_screen").removeClass("full_screen");;
                if(is_userstream != '') {
                    $scope.vc_width = "100px";
                    $scope.vc_marginleft = "100px";
                    $("#video_chat").css({
                        'width': $scope.vc_width,
                        'marginleft': $scope.vc_marginleft
                    });
                    $("#local_stream").css("display","none");
                }
                $scope.audio_notification = true;
                $('#remote_streams').removeClass("video_container").addClass("local_stream");
                $('.surfly-chat-window .content-bar .img-cont').css({"height" : "", "background-color" : "#FFFFFF"});
                $('.surfly-chat-window .content-bar .session-content-1').css("height","");
                $('.surfly-chat-window .chat-window .scroll-bar-2').css("height","");

                $scope.tc_height = "100%";
                $scope.tc_position = "relative";
                $scope.tc_bottom = "";
                $("#text_chat").css({
                    'height': $scope.tc_height,
                    'position': $scope.tc_position,
                    'bottom': $scope.tc_bottom
                });
                $scope.is_tc_hide = false;
                var div_height = (scroll != '' && scroll != undefined) ? "100%" : $("#text_chat")[0].scrollHeight;
                $("#text_chat").scrollTop(div_height);
            }
        }

        // Block text chat window after 10secs when video mode
        // enabled
        $scope.BlockChatBox = function() {
            $scope.ToggleClose();
        }

        // Block text chat window when user clicks close icon - when
        // video mode enabled
        $scope.text_chat_cancel = function() {
            $scope.ToggleClose();
        }

        // blocking textchat functionality when video mode enabled
        $scope.ToggleClose = function() {
            if (!is_video) {
                return false;
            }
            //broadcastMsg('DISPLAY_USER_NOTIFICATION');
            if ($scope.time_out_var != null)
                $timeout.cancel($scope.time_out_var);
            is_text = false;
            $scope.vc_height = "100%";
            $scope.vc_width = "100%";
            $scope.vc_marginleft = "";
            $("#video_chat").css({
                'height': $scope.vc_height,
                'width': $scope.vc_width,
                'marginleft': $scope.vc_marginleft
            }).addClass("full_screen").removeClass("small_screen");
            $('#remote_streams').removeClass("local_stream").addClass("video_container");
            $('.surfly-chat-window .content-bar .img-cont').css({"height" : "50%", "background-color" : "#000000"});
            $('.surfly-chat-window .content-bar .session-content-1').css("height","50%");
            $('.surfly-chat-window .chat-window .scroll-bar-2').css("height","50px");

            $scope.tc_height = "0%";
            $("#text_chat").css({
                'height': $scope.tc_height,
                'position': $scope.tc_position,
                'bottom': $scope.tc_bottom
            });
            $("#local_stream").css("display","block");

            $scope.tc_class = "";
            $scope.is_tc_hide = $scope.is_cancel_hide = true;
            $scope.vc_tc_class = "";
            $scope.vc_class = "content-img";
            $scope.tc_scroll_class = "";
            var div_height = $("#text_chat")[0].scrollHeight;
            $("#text_chat").scrollTop(div_height + 1);
        }
    }
    // ---- 5-> Ends here ----//

    // ---- 6.Drag starts here ----//
    if (!docked_only) {
        $scope.local_drag = false;
        $("#widget").draggable({
            //containment: "window",
            containment: $("#cobrowsing"),
            scroll: false

        });

        $("#widget").on("dragstart", function(event, ui) {
            $scope.$apply(function() {
                $scope.is_overlay_hide = false;
                $scope.box_right = "";
            });
        }).on("drag", function(event, ui) {
            $scope.$apply(function() {
                $scope.is_overlay_hide = false;
                if ($scope.is_dock)
                    $("#widget").trigger("mouseup");

            });
        }).on("dragstop", function(event, ui) {
            if ($scope.is_dock) {
                $scope.$apply($scope.chatBoxDock());
            } else {
                if (!$scope.local_drag) {
                    $scope.box_top = ui.position.top + "px";
                    $scope.box_left = ui.position.left + "px";
                    $scope.box_bottom = $(window).height() - $("#cobrowsing").height + "px";
                    $scope.$apply(function() {
                        var points = $scope.drag_sync(ui.position.top, ui.position.left);
                        broadcastMsg("USER_DRAG_WINDOW:" + points);
                    });
                }
                $scope.local_drag = false;
            }

        });

        //To get the top and left position of chatbox in percentage
        $scope.drag_sync = function(top, left, is_send) {
            var win_h = $scope.custom_height;
            var win_w = $scope.custom_width;
            //var orig_top=($(window).height()-$scope.custom_height)/2;
            var orig_top = 0;
            var orig_left = ($(window).width() - $scope.custom_width) / 2;
            var top_per = (top * 100) / win_h;
            var left_per = (left * 100) / win_w;
            top_per = (top_per > 0) ? ((((top - orig_top) + parseInt($scope.box_height)) * 100) / win_h) : 0;
            left_per = (left_per > 0) ? ((((left - orig_left) + parseInt($scope.box_width)) * 100) / win_w) : 0;
            top_per = (top_per <= 100) ? top_per : 100;
            left_per = (left_per <= 100) ? left_per : 100;
            return top_per + ":" + left_per;
        }

        //To set the bottom position of chatbox to start the animation from bottom
        $scope.SetBottom = function() {
            var bottom = GetHeightService.getWindowHW()[1] - ($('#widget').position().top + $('#widget')
                .outerHeight(true));
            $scope.box_bottom = bottom + "px";
            $scope.box_top = "";
        }

        //To set the bottom position of chatbox after animation has ended
        $scope.SetTop = function() {
            $scope.box_top = $('#widget').position().top + "px";
            $scope.box_bottom = "";
        }

        //Draggable option for user's own stream(small div)
        $("#local_stream").draggable({
            containment: "parent",
            scroll: false,
            refreshPositions: true,
            iframeFix: true
        });

        $("#local_stream").on("dragstop", function(event, ui) {
            $scope.local_drag = true;
        });

        //Sync drag functionality for follower
        $scope.drag_sync_follower = function(ui) {
            var win_h = $scope.custom_height;
            var win_w = $scope.custom_width;
            var h_per = (ui[1] > 0) ? (ui[1] - ((parseInt($scope.box_height) * 100) / win_h)) : 0;
            var w_per = (ui[2] > 0) ? (ui[2] - ((parseInt($scope.box_width) * 100) / win_w)) : 0;
            h_per = (h_per > 0) ? h_per : 0;
            w_per = (w_per > 0) ? w_per : 0;
            var top_per = (h_per * win_h) / 100;
            var left_per = (w_per * win_w) / 100;
            //var orig_top = ($(window).height() - scope.custom_height) / 2;
            var orig_top = 0;
            var orig_left = ($(window).width() - $scope.custom_width) / 2;
            orig_left = (orig_left > 0) ? orig_left : 0;
            $scope.box_top = (top_per + orig_top) + "px";
            $scope.box_left = (left_per + orig_left) + "px";
            $scope.box_bottom = "";
            $scope.box_right = "";
        }

        $scope.flash_container_value = function(str, increase){
            return parseInt(str)+increase;
        }
    }
    // ---- 6 -> Ends here ----//

    // ---- 7.Tooltip - starts here----//
    // Tooltip functionality in docked mode
    var normal_header_ids = "#no_of_users,#add_user,#switch_control,#privacy_mode,#nav_to_new_url,#end_session,#chat_box_dock_aft";
    var dock_header_ids = "#no_of_users_undock,#add_user_undock,#switch_control_undock,#privacy_mode_undock,#nav_to_new_url_undock,#end_session_undock,#chat_box_dock_b4";
    var header_icon_classes = ".user-icon,.icon-1,.icon-2,.icon-3,.icon-4,.icon-5,.icon-6";
    var footer_icon_ids = "#init_video_chat,.mic_control,#share_document_icon";
    // Widget header icon's id array
    var dock_ids = ["no_of_users", "add_user",
        "switch_control", "privacy_mode", "nav_to_new_url",
        "end_session", "chat_box_dock_aft"
    ];
    // tooltip for header icons in docked mode
    if(dock_top_position) {
        $(normal_header_ids).tooltip({
            selector: '',
            placement: 'bottom',
            container: 'body'
        });
    } else {
        $(normal_header_ids).tooltip({
            selector: '',
            placement: 'right',
            container: 'body'
        });
    }

    // tooltip for header icons in normal mode
    $(dock_header_ids).tooltip({
        selector: '',
        placement: 'auto',
        container: 'body'
    });

    // tooltip for footer icons
    $(footer_icon_ids).tooltip({
        selector: '',
        placement: 'auto',
        container: 'body'
    });

    // Removing tooltip when mouse leaves from icon
    $(normal_header_ids + dock_header_ids + footer_icon_ids).on('mouseleave',
        function() {
            $(this).tooltip('hide');
        }).on('mouseenter', function() {
        $('[rel=tooltip]').tooltip('hide');
        $(this).tooltip('show');
    });

    // Adding mouseover event for Widget header
    $("#chat_aft_dock").mouseover(function() {
        mouseOver(this.id);
    }).mouseout(function() {
        mouseOut(this.id);
    });

    // Adding mouseover event for widget header icons
    $(header_icon_classes).mouseover(function() {
        mouseOver(this.getAttribute("data-val"), this);
    }).mouseout(function() {
        mouseOut(this.getAttribute("data-val"), this)
    });

    // To reduce the opacity of all header icons other than the
    // hovered icon when it
    // is in dock mode
    function mouseOver(id, element) {
        if (id != null) {
            if (id == "chat_aft_dock") {
                $("#chat_aft_dock").css("opacity", "1");
            } else {
                $("#chat_aft_dock").find("div[data-val]").css("opacity", "0.5");
                $(element).css("opacity", "1");
            }
        }
    }

    // To increase the opacity of hovered icon when it is in
    // dock mode
    function mouseOut(id, element) {
            if (id != null) {
                if (id == "chat_aft_dock") {
                    $("#chat_aft_dock").css("opacity", "0.5");
                    $("#chat_aft_dock").find("div[data-val]").css("opacity", "1");
                } else {
                    $(element).css("opacity", "0.5");
                }
            }
        }
        // ---- 7 -> Ends Here ----//

    // ---- 8.Resize - starts here ----//
    if (!docked_only) {
        $("#widget").on("resizestart", function(event, ui) {
            $scope.$apply(function() {
                $scope.is_overlay_hide = false;
                $scope.sizeAdjust();
            });
            $("#widget").draggable("disable");
            $scope.is_increase = false;
        }).on("resize", function(event, ui) {
            var dh = $("#dialog_footer").height();
            if (ui.originalSize.width > ui.size.width && ui.size.width < ($("#local_stream").position().left + $("#local_stream").width())) {
                $("#local_stream").css({
                    "left": "auto",
                    "right": "0px"
                });
            }
            if (ui.originalSize.height > ui.size.height && (ui.size.height - dh) < ($("#local_stream").position().top + $("#local_stream").height())) {
                $("#local_stream").css({
                    "top": "auto",
                    "bottom": dh + "px"
                });
            }
            $scope.$apply(function() {
                $scope.is_overlay_hide = false;
                $scope.sizeAdjust();
                if ($scope.is_dock)
                    $("#widget").trigger("mouseup");

            });
            $("#widget").draggable("disable");
        }).on("resizestop", function(event, ui) {
            if ($scope.is_dock) {
                $scope.$apply($scope.chatBoxDock());
            } else {
                $scope
                    .$apply(function() {
                        $scope.box_top = ui.position.top + "px";
                        $scope.box_left = ui.position.left + "px";
                        $scope.box_height = ui.size.height + "px";
                        $scope.temp_wid_height = ui.size.height;
                        $scope.box_width = ui.size.width + "px";
                    });
                $scope
                    .$apply(function() {
                        $scope.sizeAdjust();
                        var points = $scope.drag_sync(ui.position.top, ui.position.left);
                        var per = $scope.resize_sync_init(ui.size.height, ui.size.width);
                        $scope
                            .broadcastMsg("USER_RESIZE_WINDOW:" + points + ":" + per);
                    });
                $("#widget").draggable("enable");
            }

        });
    }

    // Size Adjustment when resizing
    $scope.sizeAdjust = function() {
        $scope.dialog_content_style = {
            'height': (GetHeightService.CalculateHeight() + "%")
        };
        $scope.dialog_videowtext_chat_window_style = {
            'height': "100%"
        };
        if (is_video && is_text) {
            var dialog_h = $("#dialog_videowtext_chat_window")
                .height();
            var close_h = 100 - ((16 / dialog_h) * 100);
            $scope.vc_height = "0%";
            $scope.tc_height = "100%";
            $("#videochat").addClass("small_screen").removeClass("full_screen");
        } else if (is_video && !is_text) {
            $scope.vc_height = "100%";
            $("#videochat").addClass("full_screen").removeClass("small_screen");
        } else {
            $scope.tc_height = "100%";
        }
    }

    //To get the width and height in percentage
    $scope.resize_sync_init = function(h, w) {
        var h_per = (h * 100) / $scope.custom_height;
        var w_per = (w * 100) / $scope.custom_width;
        return h_per + ":" + w_per;
    }

    //Resize sync
    $scope.resize_sync = function(h_per, w_per) {
        var win_h = $scope.custom_height;
        var win_w = $scope.custom_width;
        var h = (win_h * h_per) / 100;
        var w = (win_w * w_per) / 100;
        h = (h > 275) ? h : 275;
        w = (w > 250) ? w : 250;
        $scope.box_height = h + "px";
        $scope.box_width = w + "px";
        $scope.temp_wid_height = h;
    }



    // ---- 8 -> Ends here ----//

    // ---- 9.Indicating user to update his/her IE browser -
    // starts here ----//
    $(document)
        .ready(
            function() {
                if (navigator.appName == "Microsoft Internet Explorer") {
                    msie = parseInt(navigator.appVersion
                        .match(/MSIE ([\d.]+)/)[1]);
                    if (navigator.appName == "Microsoft Internet Explorer" && msie < 10) {
                        noty({
                            text: "Please upgrade to a more modern browser",
                            type: "warning",
                            timeout: "15000"
                        });
                    }
                }

                $(window).resize(function(e) {
                    if (e.target == window && (is_owner || set_to_smallest))
                        $scope.$apply($scope.sendWH_Leader());
                    else
                        $scope.$apply($scope.CustomSyncFollower());
                    $scope.$apply($scope.sizeAdjust());
                });

                $("#cobrowsing").on("load", function() {
                    // do something once the iframe is loaded
                    $(".yoursession-content").addClass('session-contentwstroke');
                });
            });
    // ---- 9 - ends here ----//

    // ---- 10.Video full screen mode - starts here --//
    if (!docked_only) {
        $scope.full_screen = function() {
            $scope.full_screen_color = "#000000";
            if (is_owner) {
                $scope.full_screen_display = true;
                broadcastMsg("VIDEO_CHAT_MAXIMIZE");
                maximize_streams($scope.is_spectator_mode);
                $scope.resize_video_streams();
            }
        }

        // Return to normal mode when user clicks on video
        $scope.normal_screen = function() {
            if (is_owner) {
                $scope.full_screen_display = false;
                broadcastMsg("VIDEO_CHAT_MINIMIZE");
                minimize_streams($scope.is_spectator_mode);
                $scope.resize_video_streams();
            }
        }

        // Return to normal mode when user presses 'Esc' key
        $scope.normal_screen_esc = function(keyCode) {
            if (is_owner && keyCode == 27) {
                $scope.full_screen_display = false;
                broadcastMsg("VIDEO_CHAT_MINIMIZE");
                minimize_streams($scope.is_spectator_mode);
                $scope.resize_video_streams();
            }
        }
    } else {
        $scope.chatBoxDock();
    }
    // ---- 10 - ends here ----//

    // ---- 11.iframe size settings ---- //
    //get width and height
    $scope.sendWH_Leader = function() {
        var width = $window.innerWidth;
        var height = $window.innerHeight;
        if (is_owner) {
            $scope.window_width_list["user0"] = width;
            $scope.window_height_list["user0"] = height;
            $scope.calculateCustom();
        } else {
            broadcastMsg("ORIGINAL_WIDTH_HEIGHT:" + width + ":" + height);
        }
    }

    /*Calculate top left points from given attributes
        -box_right,box_bottom,box_width,box_height
    */
    $scope.getTopLeftpoints = function(){
        $scope.custom_height = ($scope.custom_height > $(window).height()) ? $(window).height() : $scope.custom_height;
        $scope.custom_width = ($scope.custom_width > $(window).width()) ? $(window).width() : $scope.custom_width;
        if($scope.box_bottom.indexOf("px") >= 0)
        {
            bxbottom = parseFloat($scope.box_bottom.replace("px", ""));
            bxbottom_per = (bxbottom / $scope.custom_height) * 100;
        }
        else
        {
            bxbottom_per = parseFloat($scope.box_bottom.replace("%", ""));
        }
        if($scope.box_right.indexOf("px") >= 0)
        {
            bxright = parseFloat($scope.box_right.replace("px", ""));
            bxright_per = (bxright / $scope.custom_width) * 100;
        }
        else
        {
            bxright_per = parseFloat($scope.box_right.replace("%", ""));
        }
        var top = $scope.custom_height - ((($scope.custom_height * bxbottom_per) / 100) + parseInt($scope.box_height));
        var left = $scope.custom_width - ((($scope.custom_width * bxright_per) / 100) + parseInt($scope.box_width));
        return ($scope.drag_sync(top, left)).split(":");
    };

    /*Calculate custom_width and custom_height based on 4 cases
        1.set_to_smallest = true but min/max have values-> consider leader and all follower's window size.
        2.set_to_smallest = false but min/max have values -> consider only leaders window size
        3.set_to_smallest = false and min/max =nul -> set leaders window size as iframe size
        4.set_to_smallest = true but min/max =null -> consider leader and all follower's window size
    */

    $scope.calculateCustom = function() {
        var points = [0, 0];
        var per = [0, 0];
        if (!docked_only) {
            if ($scope.box_right == "") {
                $scope.box_right_per = (50 / $scope.custom_width) * 100;
                $scope.box_bottom_per = (30 / $scope.custom_height) * 100;
                $scope.box_top = $scope.custom_height - ((($scope.custom_height * $scope.box_bottom_per) / 100) + parseInt($scope.box_height));
                $scope.box_left = $scope.custom_width - ((($scope.custom_width * $scope.box_right_per) / 100) + parseInt($scope.box_width));
                points = ($scope.drag_sync(parseInt($scope.box_top), parseInt($scope.box_left))).split(":");
            } else {
                points = $scope.getTopLeftpoints();
            }
            per = ($scope.resize_sync_init(parseInt($scope.box_height), parseInt($scope.box_width))).split(":");
        }
        if (min_width != 0 || max_width != 0 || min_height != 0 || max_height != 0) {
            var is_w = false;
            var is_h = false;
            //for width
            if (min_width != 0) {
                angular.forEach($scope.window_width_list, function(value, key) {
                    if (value < min_width) {
                        $scope.custom_width = min_width;
                        is_w = true;
                    }
                });
            }
            if (!is_w) {
                var min_val = 0;
                if ($scope.window_width_list.length > 0)
                    min_val = $scope.window_width_list[0];
                angular.forEach($scope.window_width_list, function(value, key) {
                    if (min_val > value || min_val == 0) {
                        min_val = value;
                    }
                });

                $scope.custom_width = ((min_val > max_width) && (max_width != 0)) ? max_width : min_val;
            }
            //for height
            if (min_height != 0) {
                angular.forEach($scope.window_height_list, function(value, key) {
                    if (value < min_height) {
                        $scope.custom_height = min_height;
                        is_h = true;
                    }
                });
            }
            if (!is_h) {
                var min_val = 0;
                if ($scope.window_height_list.length > 0)
                    min_val = $scope.window_height_list[0];
                angular.forEach($scope.window_height_list, function(value, key) {
                    if (min_val > value || min_val == 0) {
                        min_val = value;
                    }
                });

                $scope.custom_height = ((min_val > max_height) && (max_height != 0)) ? max_height : min_val;
            }
        } else if (set_to_smallest) {
            var min_val_w = 0;
            if ($scope.window_width_list.length > 0)
                min_val_w = $scope.window_width_list[0];
            angular.forEach($scope.window_width_list, function(value, key) {
                if (min_val_w > value || min_val_w == 0) {
                    min_val_w = value;
                }
            });

            var min_val_h = 0;
            if ($scope.window_height_list.length > 0)
                min_val_h = $scope.window_height_list[0];
            angular.forEach($scope.window_height_list, function(value, key) {
                if (min_val_h > value || min_val_h == 0) {
                    min_val_h = value;
                }
            });
            $scope.custom_width = min_val_w;
            $scope.custom_height = min_val_h;

        } else {
            $scope.custom_width = $window.innerWidth;
            $scope.custom_height = $window.innerHeight;
        }
        $("#cobrowsing,body").width($scope.custom_width + "px").height($scope.custom_height + "px");
        $scope.custom_width_height = $scope.custom_width + ":" + $scope.custom_height;
        if (!docked_only) {
            $scope.drag_sync_follower([0, points[0], points[1]]);
        }
        var is_dock = "false";
        if ($scope.is_dock)
            is_dock = "true";
        if ((parseInt($scope.box_height) > $scope.custom_height) || (parseInt($scope.box_width) > $scope.custom_width)) {
            if (!docked_only)
                $scope.resize_sync(per[0], per[1]);
            //We have to pass custom width and hight to followers
            broadcastMsg("CUSTOM_HEIGHT_HEIGHT:" + points[0] + ":" + points[1] + ":" + per[0] + ":" + per[1] + ":" + $scope.custom_width_height + ":" + 0 + ":" + 0 + ":" + is_dock);

        } else {
            //We have to pass custom width and hight to followers
            broadcastMsg("CUSTOM_HEIGHT_HEIGHT:" + points[0] + ":" + points[1] + ":" + 0 + ":" + 0 + ":" + $scope.custom_width_height + ":" + parseInt($scope.box_width) + ":" + parseInt($scope.box_height) + ":" + is_dock);
        }

        if ($scope.is_dock) {
            $scope.chatBoxDock();
        }
    }

    // ---- 11- ends here ---- //

    $scope.CustomSyncFollower = function() {
        var points = [0, 0];
        var per = [0, 0];
        /*$scope.custom_height = ($scope.custom_height > $(window).height()) ? $(window).height() : $scope.custom_orig_height;
        $scope.custom_width = ($scope.custom_width > $(window).width()) ? $(window).width() : $scope.custom_orig_width;*/
        if (!docked_only || !$scope.is_dock) {
            if ($scope.box_right == "") {
                points = ($scope.drag_sync(parseInt($scope.box_top), parseInt($scope.box_left))).split(":");
            } else {
                points = $scope.getTopLeftpoints();
            }
            per = ($scope.resize_sync_init(parseInt($scope.box_height), parseInt($scope.box_width))).split(":");
        }
        if (!docked_only || !$scope.is_dock) {
            $scope.drag_sync_follower([0, points[0], points[1]]);
        }
        if ((parseInt($scope.box_height) > $scope.custom_height) || (parseInt($scope.box_width) > $scope.custom_width)) {
            if (!docked_only || !$scope.is_dock)
                $scope.resize_sync(per[0], per[1]);
        }
        if ($scope.is_dock) {
            $scope.chatBoxDock();
        }
    }

    // ---- 12.common functionalities ---- //
    // To open textchat page for followers
    if (is_owner && !ui_off && (sharing_button || splash)) {
        $scope.page = "dialog_session_start";
    } else {
        $scope.timerRef = 0;
        $scope.SessionActivated();
    }

    $scope.showfirsttimeonly = function(session_id)
    {
        if(!(sessionStorage && sessionStorage.getItem(session_id + 'invited')) && (!meeting_session) && !ui_off) {
            splash == true ? $scope.showModal() : $scope.hideModal();
        }
    };

    //To calculate video div width
    $scope.resize_video_streams = function() {
        var l = $("#remote_streams .user_streams").not("[is_spectator]").length;

        ustream_width = (l > 1) ? "50" : "100";
        $(".user_streams").not("[is_spectator]").width(ustream_width + "%");
        rrows = Math.ceil(l/2);
        ustream_height = 100/rrows;
        $(".user_streams").not("[is_spectator]").height(ustream_height + "%");
        $scope.init_video_chat();
    }

    var textSelect = function(){
        $('.user-comments').hover(function(){
            $('#widget').draggable('disable');
            $(this).css('cursor', 'text');
        }, function(){
            $("#widget").draggable("enable");
        });
    }



    $scope.$on('ngRepeatFinished', function(ngRepeatFinishedEvent) {
        var div_height = $("#text_chat")[0].scrollHeight;
        $('#text_chat').animate({scrollTop:  div_height});
        textSelect();

        $(".url_in_text").on('click', function (e) {
            e.preventDefault();
            if(is_owner)
                $scope.Navigate($(this).attr('href'), $(this).text());
            else
                return false;
        });
    });
    // ---- 12 - ends here ---- //

    /* ******New Conference changes starts here********** */
    $scope.invite_conference = false;
    // Videochat options
    // 1 - video, 2 - audio, 3 - phone, 4 - chat
    $scope.selected_option = 0;
    $scope.session_started = false;
    $scope.isFlashSession = RTCPeerConnection ? false : true;
    $scope.show_conference_popup = function(){
        if(videochat)
        {
            if (meeting){
               $scope.invite_conference = true;
               angular.element(".avgrund-cover").addClass("conf_popup");
           } else{
               $scope.conference_option(1);
           }
        }
    };

    $scope.conference_option = function(opt){
        $scope.selected_option = opt;
        $scope.invite_conference = false;
        angular.element(".avgrund-cover").removeClass("conf_popup");

        if(opt == 1 || opt == 2)
        {
            initSDK();
        }
        else if(opt == 4){
            $scope.vicon_tooltip_text = 'Start videochat';
            if(!$scope.is_conf_initiator){
                initSDK();
            }
            $scope.is_conf_initiator = false; /* Specially for initiator clicked and decided not to activate conf (by clicking X button) */
        }
    };

    /* ******New Conference changes ends here********** */
});

//Service to get window width and height
mod.factory("GetHeightService", function() {
    var height = {};
    /* Get window height and width for IE and Other Browser */
    height.getWindowHW = function() {
        if (window.innerWidth != undefined) {
            return [window.innerWidth, window.innerHeight];
        } else {
            var B = document.body,
                D = document.documentElement;
            return [Math.max(D.clientWidth, B.clientWidth),
                Math.max(D.clientHeight, B.clientHeight)
            ];
        }
    }

    /* Calculate dialog content height */
    height.CalculateHeight = function() {
        return (100 - ((95 / $("#widget").height()) * 100));
    }
    return height;
});

//To make links in the text
mod.filter('linkyWithHtml', function($filter) {
  return function(value) {
    var linked = $filter('linky')(value).replace(/<a /g, '<a class="url_in_text" ');
    return linked;
  };
});

// To Scroll to last chat message
mod.directive('messageRepeat', function($timeout) {
    return {
        restrict: 'A',
        link: function(scope, element, attr) {
            if (scope.$last === true) {
                $timeout(function() {
                    scope.$emit('ngRepeatFinished');
                });
            }
        }
    }
});
// To make keypress functionality
mod.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });

                event.preventDefault();
            }
        });
    };
});

//To set resizable options
$(document).ready(function() {
    if (!docked_only) {
        var mh = parseInt($("#widget").css("min-height"), 10);
        var mw = parseInt($("#widget").css("min-width"), 10);
        $("#widget").resizable({
            handles: "ne, se, sw, nw",
            minHeight: mh,
            minWidth: mw,
            containment: $("#cobrowsing")
        });
    }
});

// Common global variables
var active;
var path = location.pathname;
$('input[rel=tooltip], label[rel=tooltip]').tooltip({
    trigger: "manual",
    placement: "right"
}).tooltip("show");
