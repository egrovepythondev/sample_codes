class WIDGETAPI_v2_Test(TransactionTestCase):

    def setUp(self):
        self.user = User.objects.create(email='hello@world.com', password='example')
        self.company = Company.objects.create(name='AceMe', client_key='00063668234b4aa19dd436e6a76ab8ae')
        self.agent = Agent.objects.create(user=self.user, company=self.company, is_company_admin=True)
        self.apikey = APIKey.objects.create(user=self.user, company=self.company, apikey='3b57bd2db21c46d2875508b982968d26',
                                            widget_key='5078989820ff6b44c12ea423113788')
        self.reseller = ReSeller.objects.create(user=self.user, reseller_key='02fbfd63b54341c1828e6a2426311788')
        self.domains = CompanyDomains.objects.create(company=self.company, domains="*.google.com, google.com")
        self.plan = PlanPricing.objects.create(name='pro')
        PlanPricing.objects.create(name='business')
        self.auth = (self.apikey.apikey, '')
        self.widgetauth = (self.apikey.widget_key, '')

    def test_cors_request_wrong_key(self):
        result = requests.post(APIv2 + 'sessions/?api_key=' + self.apikey.apikey,
                               headers={"origin": "http://www.google.com"},
                               data={"url": 'http://google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 403)
        self.assertEqual(result.headers['content-type'], 'application/json')
        self.assertEqual(result.json(), {u'detail': u'Your REST key cannot be used for CORS.'})

    def test_cors_forbidden_request(self):
        result = requests.post(APIv2 + 'clients/' + self.company.client_key + '/',
                               headers={"origin": "http://google.com"},
                               auth=self.widgetauth)
        self.assertEqual(result.status_code, 403)
        self.assertEqual(result.json(), {u'detail': u'Your WIDGET key cannot be used for this type of request.'})

    def test_cors_request_apikey(self):
        result = requests.post(APIv2 + 'sessions/?api_key=' + self.apikey.widget_key,
                               headers={"origin": "http://www.google.com"},
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 200)
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)

    def test_cors_request_auth(self):
        result = requests.post(APIv2 + 'sessions/',
                               headers={"origin": "http://google.com/somepage.html"},
                               auth=self.widgetauth,
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 200)
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)

    def test_cors_request_invalid_domain(self):
        result = requests.post(APIv2 + 'sessions/',
                               headers={"origin": "https://www.google.com"},
                               auth=self.widgetauth,
                               data={"url": 'http://www.facebook.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 403)
        self.assertEqual(result.json(), {u'detail': u'Domain is not allowed by API key'})

    def test_cors_request_missing_origin(self):
        result = requests.post(APIv2 + 'sessions/',
                               auth=self.widgetauth,
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 403)
        self.assertEqual(result.json(), {u'detail': u'Your request is missing an origin header'})

    def test_cors_request_invalid_origin(self):
        result = requests.post(APIv2 + 'sessions/',
                               headers={"origin": "www.facebook.com"},
                               auth=self.widgetauth,
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 403)
        self.assertEqual(result.json(), {u'detail': u'Your request is originating from an invalid origin'})

    def test_cors_request_auth_clientkey_optional(self):
        result = requests.post(APIv2 + 'sessions/?clientkey=' + self.company.client_key,
                               headers={"origin": "www.google.com"},
                               auth=self.widgetauth,
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 200)
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)
