// JavaScript Document
"use-strict"
angular.module('vpc.forms')
    .directive('saveForm', ['$rootScope', '$location', 'toastr', 'rest',  function($rootScope, $location, toastr, rest) {
        return {
            restrict: 'E',
            transclude: false,
            scope: {
                "detail" : "=detail",
                "successMessage" : "@successMessage",
                "url" : "@url",
                "formNameList": "=formNameList"
            },
            replace: true,
            template: "<button class='form-control btn-primary push-right' ng-click='submitData()'>Save</button>",
            controller: ["$scope", "$element", "$attrs", "$location", function($scope, $element, $attr, $location) {

                $scope.getFormName = function() {
                    var arrayVariable = [];
                    $(".compactForm").each(function(i) {
                        arrayVariable.push($(this).attr("group-name"));
                    });
                    return arrayVariable
                }
                $scope.getInputCount = function(className) {
                    return $("." + className).length
                }


                $scope.submitData = function() {
                    if($scope.$parent.canSave === undefined || $scope.$parent.canSave) {

                        //Mongo likes to use _id as it's primary key even though sequence_id is the name in our model
                        if ("_id" in $scope.$parent.details) {
                            $scope.$parent.details["sequence_id"] = $scope.$parent.details["_id"];
                            delete $scope.$parent.details["_id"];
                        }
                        rest.http({
                            "url": $location.$$path,
                            "method": "sequence_id" in $scope.$parent.details ? "PUT" : "POST", //typically POST, PUT or DELETE
                            "data": $scope.$parent.details,
                        }).then(function(data) {
                            //Show a toast based on the result received.
                            if (data.status === 200 && !data.detail.valErrors) {
                                //Ensures put works after first save
                                $scope.$parent.details["_id"] = data.detail._id;

                                toastr.success($scope.successMessage || "Changes Saved", 'Success');
                               
                                $scope.$broadcast( "saveComplete" );
                                //$scope.$parent.myForm.$setPristine();
                                //$scope.$parent.contractForm.$setPristine();
                                _.each($scope.formNameList, function(name){$scope.$parent[name].$setPristine()})
                            
                            } else if(data.status === 200 && data.detail.valErrors){
                                $scope.$parent.statusMessage = data.detail.valErrors;


                            }else{
                                toastr.error('Please Try Again', 'Error');
                            }
                        });
                    }else{

                        toastr.error('Please complete  a row to save', 'Error');
                    }
                }


            }],
            link: function(scope, element, attrs, parentCtrl) {

            },
        }
    }]);
