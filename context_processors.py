__author__= "egrove"
from datetime import datetime
import pytz
import settings
from app.models import (ReSeller, Company,
                              Agent)
from app.views import check_and_create_agent


def check_is_reseller(request, call_from_view=None):
    """
    Check the logged in user as Reseller or not and get the
    company admin user, company object and most expensive
    plan.
    """
    is_company_admin = False
    is_reseller = False
    company_obj = None
    new_page = None
    settings_page = None
    # company object for both company admin and non admin
    registered_company = None

    try:
        reseller = ReSeller.objects.get(user__id=request.user.id)
    except ReSeller.DoesNotExist:
        try:
            company_obj = Agent.objects.get(user__id=request.user.id,
                                            is_company_admin=True).company
            is_company_admin = True
        except Agent.DoesNotExist:
            pass
    else:
        is_company_admin = True
        is_reseller = True
        try:
            company_obj = Company.objects.get(id=request.session.get('reseller_company_id', 0))
        except Company.DoesNotExist:
            company_obj = reseller.company.all()[0]

    if request.user.is_authenticated():
        # Get company for non company admin also and assign in diff variable
        # Let leave company_obj only for company admin as it used for many conditions
        registered_company = company_obj
        if not registered_company:
            try:
                _auth_agent = Agent.objects.get(user__id=request.user.id)
            except Agent.DoesNotExist:
                _auth_agent = check_and_create_agent(request)
                # As this agent is a old user, add him as company admin
                is_company_admin = True
                company_obj = _auth_agent.company
            finally:
                registered_company = _auth_agent.company

        set_last_activity(request, registered_company)

    if not call_from_view:
        new_page, settings_page = get_active_page_name(request)

    data = {
        'is_reseller': is_reseller,
        'is_company_admin': is_company_admin,
        'company': company_obj,
        'registered_company': registered_company,
        'active_page': new_page,
        'settings_page': settings_page,
        'CONTACT_MAIL': settings.DEFAULT_FROM_EMAIL,
        'SEGMENT_KEY': settings.SEGMENT_KEY
    }

    return data


def get_active_page_name(request):
    """
    Convert URL to page name
    '/register/' -> 'register'
    '/profile/plan/' -> 'profile_plan'
    """
    settings_page_list = ['profile', 'profile_plan', 'customization_page',
                          'profile_api', 'billing_info', 'invoice_list']
    page = ''

    path_list = request.get_full_path().split('/')

    for path in path_list:
        if path and page:
            page += '_' + path
        else:
            page += path

    page = page.strip()

    if page.__contains__('billing_info'):
        page = 'billing_info'

    if page.__contains__('invoice_list'):
        page = 'invoice_list'

    new_page = settings_page = page

    if new_page.__contains__('usage'):
        new_page = 'usage_overview'

    if new_page.__contains__('agents'):
        new_page = 'agents'

    if new_page.__contains__('customization_page'):
        new_page = 'customization_page'

    if new_page.__contains__('login'):
        new_page = 'login'

    if new_page in settings_page_list:
        new_page = 'settings'

    return new_page, settings_page


def set_last_activity(request, company):
    """
    Set last activity of an agent in his company
    (Set current time in company evenif the user is non company admin)
    """
    company.last_activity = datetime.now(pytz.utc)
    company.save()
