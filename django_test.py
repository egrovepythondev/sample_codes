import sys
import os
sys.path.insert(0, '../util/')
import json
import pytz
import base64
from datetime import datetime, date, timedelta
from dateutil.relativedelta import relativedelta
from dateutil.parser import parse

from django.test import TransactionTestCase, Client
from django.core.urlresolvers import reverse
from faker import Factory as FakerFactory

from surflyapp.models import (Agent, ResellerMutations, QuadernoTransaction)

import subprocess
import time
import requests
from bs4 import BeautifulSoup
from icalendar import Calendar, vDatetime, vText
from surflyapp.figlo_controller import update_current_add_to_figlo_webhook
from surflyapp.util import (is_invoice_client,
                            log_and_call_add_client_webhook,
                            log_and_call_remove_client_webhook,
                            log_and_call_add_agent_webhook,
                            log_and_call_remove_agent_webhook,
                            log_and_call_custom_action_webhook,
                            create_or_update_quaderno_contact,
                            convert_datetime_by_timezone)
from surflyapp.test_data_factory import (UserFactory, PlanPricingFactory, CompanyFactory, AgentFactory,
                                         BillingInfoFactory, ReSellerFactory, APIKeyFactory, ResellerMutationsFactory,
                                         ResellerCompanyAccountsFactory, InvoiceFactory,
                                         UsageHistoryFactory, IncomingQueueFactory, ReferrerHistoryFactory,
                                         RegistrationProfileFactory,
                                         USER_PASSWORD)

APIv1 = os.environ.get('APIv1', 'https://api.sitesupport.net/v1/session/')
APIv2 = os.environ.get('APIv2', 'https://sitesupport.net/v2/')
SITE = os.environ.get('SITE', 'https://sitesupport.net/')

SERVER_PROCESS = None

faker = FakerFactory.create()


def setUpModule():
    global SERVER_PROCESS
    SERVER_PROCESS = subprocess.Popen(
        [sys.executable, 'run.py', '--test'],
        cwd='..')
    time.sleep(2)


def tearDownModule():
    _kill(SERVER_PROCESS)


class RESTAPI_v1_Test(TransactionTestCase):

    def setUp(self):
        self.user = UserFactory()
        self.apikey = APIKeyFactory(user=self.user)
        self.auth = (self.user.email, self.apikey.apikey)

    def test_200(self):
        result = requests.post(APIv1 + 'new', data={"url": 'http://www.google.com'}, auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)

    def test_405(self):
        result = requests.get(APIv1 + 'new')
        self.assertEqual(result.status_code, 405)

    def test_404(self):
        result = requests.post(APIv1 + 'xxx')
        self.assertEqual(result.status_code, 404)

    def test_401(self):
        result = requests.post(APIv1 + 'new')
        self.assertEqual(result.status_code, 401)

        result = requests.post(APIv1 + 'new/xxx')
        self.assertEqual(result.status_code, 401)

        result = requests.post(APIv1 + 'new', data={"url": 'http://www.google.com'}, auth=('', ''))
        self.assertEqual(result.status_code, 401)
        self.assertEqual(result.headers['content-type'], 'application/json')

    def test_400_missingurl(self):
        result = requests.post(APIv1 + 'new', auth=self.auth)
        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.headers['content-type'], 'application/json')
        self.assertEqual(result.json(), {u'error': u'missing required argument: url'})

    def _test_400_badurl(self, url, error):
        result = requests.post(APIv1 + 'new', data={"url": url}, auth=self.auth)
        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.headers['content-type'], 'application/json')
        self.assertEqual(result.json(), {u'error': u'Failed to parse url: %s' % error})

    def _disabled_test_400_badurl(self):
        self._test_400_badurl("ftp://google.com", 'Only "http" and "https" schemes are allowed')
        self._test_400_badurl("http://google.com:81/", 'Non-standard ports are not supported')


class RESTAPI_v2_Test(TransactionTestCase):

    def setUp(self):
        self.company = CompanyFactory(domains="*.google.com, google.com")
        self.agent = AgentFactory(company=self.company)
        self.apikey = APIKeyFactory(user=self.agent.user, company=self.company)
        self.reseller = ReSellerFactory(user=self.agent.user)
        self.plan = PlanPricingFactory()
        self.auth = (self.apikey.apikey, '')
        self.widgetauth = (self.apikey.widget_key, '')

    # Session creation

    def test_200_optional_apikey(self):
        result = requests.post(APIv2 + 'sessions/?api_key=' + self.apikey.apikey,
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)

    def test_200_authorization(self):
        result = requests.post(APIv2 + 'sessions/',
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)

    def test_200_widgetauth(self):
        result = requests.post(APIv2 + 'sessions/',
                               headers={"origin": "http://www.google.com"},
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id},
                               auth=self.widgetauth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)

    def test_403_no_key(self):
        result = requests.post(APIv2 + 'sessions/',
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 403)
        self.assertEqual(result.headers['content-type'], 'application/json')
        json_obj = result.json()
        self.assertEqual(json_obj['detail'], u"No API key specified.")

    def test_403_invalid_key(self):
        result = requests.post(APIv2 + 'sessions/',
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id},
                               auth=("bla", "bla"))
        self.assertEqual(result.status_code, 403)
        self.assertEqual(result.headers['content-type'], 'application/json')
        json_obj = result.json()
        self.assertEqual(json_obj['detail'], u"Invalid API key!")

    def test_missing_paramaters(self):
        result = requests.post(APIv2 + 'sessions/',
                               auth=self.auth)
        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.headers['content-type'], 'application/json')
        json_obj = result.json()
        self.assertEqual(json_obj['detail'], u"url: This field is required.")
        #self.assertEqual(json_obj['error']['agent_id'][0], u"This field is required.")

    def test_missing_agent_id(self):
        result = requests.post(APIv2 + 'sessions/',
                               data={"url": 'http://www.google.com'},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)

    def test_missing_url(self):
        result = requests.post(APIv2 + 'sessions/',
                               data={"agent_id": self.agent.id},
                               auth=self.auth)
        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.headers['content-type'], 'application/json')
        json_obj = result.json()
        self.assertEqual(json_obj['detail'], u"url: This field is required.")

    def _test_400_badurl(self, url, error):
        result = requests.post(APIv2 + 'sessions/?api_key=' + self.apikey.apikey,
                               data={"url": url, "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.headers['content-type'], 'application/json')
        self.assertEqual(result.json(), {u'detail': u'Failed to parse url: %s' % error})

    def test_400_badurl(self):
        self._test_400_badurl("ftp://google.com", 'Only "http" and "https" schemes are allowed')
        self._test_400_badurl("http://google.com:81/", 'Non-standard ports are not supported')

    def test_session_list(self):
        def _check_session(session):
            self.assertFalse('leader_link' in session)
            self.assertTrue('viewer_link' in session)
            self.assertTrue('agent_id' in session)
            self.assertTrue('session_id' in session)
            self.assertTrue('start_time' in session)
            self.assertTrue('duration' in session)
        # create session
        result = requests.post(APIv2 + 'sessions/',
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        # Create a new session
        result = requests.post(APIv2 + 'sessions/',
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id},
                               auth=self.auth)
        # check session list
        result = requests.get(APIv2 + 'sessions/',
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        session_list = result.json()
        self.assertEqual(len(session_list), 2)
        for session in session_list:
            _check_session(session)
        # get specific session info
        session_id = session_list[0]['session_id']
        result = requests.get(APIv2 + 'sessions/' + session_id + "/",
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        session = result.json()

    def test_optional_name_email(self):
        # comon request for all session link
        leader_request_session = requests.Session()
        follower_request_session = requests.Session()

        def get_script_data(_link, leader=True):
            if leader:
                _page = leader_request_session.get(_link).content
            else:
                _page = follower_request_session.get(_link).content
            return BeautifulSoup(_page, 'html.parser').find(id="set_variable").text

        result = requests.post(APIv2 + 'sessions/',
                               data={"url": 'http://www.google.com'},
                               auth=self.auth)

        leader_link = "{}?name=leader_set_his_name&email=leader_set_his_name@surfly.com".format(result.json()["leader_link"])
        script_text = get_script_data(leader_link)
        self.assertIn("leader_set_his_name", script_text)
        self.assertIn("leader_set_his_name@surfly.com", script_text)

        viewer_link = "{}?name=viewer_set_his_name&email=viewer_set_his_name@surfly.com".format(result.json()["viewer_link"])
        script_text = get_script_data(viewer_link, False)
        self.assertIn("viewer_set_his_name", script_text)
        self.assertIn("viewer_set_his_name@surfly.com", script_text)

        # test with overwrite name and email
        viewer_link = "{}?name=viewer_new_name&email=viewer_new_name@surfly.com".format(result.json()["viewer_link"])
        script_text = get_script_data(viewer_link, False)
        self.assertIn("viewer_set_his_name", script_text)
        self.assertIn("viewer_set_his_name@surfly.com", script_text)
        self.assertNotIn("viewer_new_name", script_text)
        self.assertNotIn("viewer_new_name@surfly.com", script_text)


class RESTAPI_v2_session(TransactionTestCase):

    def setUp(self):
        self.agent = AgentFactory()
        self.agent_new = AgentFactory(company=self.agent.company)
        self.apikey = APIKeyFactory(user=self.agent.user, company=self.agent.company)
        self.reseller = ReSellerFactory(user=self.agent.user)
        self.plan = PlanPricingFactory()
        self.auth = (self.apikey.apikey, '')
        self.widgetauth = (self.apikey.widget_key, '')
        self.client_key = self.agent.company.client_key

    def test_create_session_check_list(self):
        def _check_session(session):
            self.assertFalse('leader_link' in session)
            self.assertTrue('viewer_link' in session)
            self.assertTrue('agent_id' in session)
            self.assertTrue('session_id' in session)
            self.assertTrue('start_time' in session)
            self.assertTrue('duration' in session)
            self.assertTrue('queue_id' in session)

        # Create Session
        result = requests.post(APIv2 + 'sessions/',
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

        result = requests.post(APIv2 + 'sessions/',
                               data={"url": 'http://www.yahoo.com', "agent_id": self.agent.id},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

        # Create a new session with new user
        result = requests.post(APIv2 + 'sessions/',
                               data={"url": 'http://surfly.com', "agent_id": self.agent_new.id},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

        # Check Session List
        result = requests.get(APIv2 + 'sessions/',
                              auth=self.auth)
        session_list = result.json()
        session_id = session_list[0]['session_id']
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        # Totally three sessions available
        self.assertEqual(len(session_list), 3)
        for session in session_list:
            _check_session(session)

        result = requests.get(APIv2 + 'sessions/?agent_id=' + str(self.agent.id),
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        session_list = result.json()
        # self.user have two sessions
        self.assertEqual(len(session_list), 2)
        for session in session_list:
            _check_session(session)

        # End session
        result = requests.delete(APIv2 + 'sessions/' + session_id + '/',
                                 auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        self.assertEqual(result.json(), {u'response': u'Session has been ended successfully'})

        # Get the Active Session
        result = requests.get(APIv2 + 'sessions/?active_session=true',
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        session_list = result.json()
        #Totally 3 session, One session is ended
        self.assertEqual(len(session_list), 2)
        for session in session_list:
            _check_session(session)

        # Get the Active Session for a certain Agent
        session = session_list[0]
        start_time = session['start_time'].split('-')

        data = 'agent_id={0}&active_session={1}&year={2}&month={3}'.format(
            self.agent.id, True, start_time[0], start_time[1])

        result = requests.get(APIv2 + 'sessions/?' + data,
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        session_list = result.json()
        for session in session_list:
            _check_session(session)

        result = requests.get(APIv2 + 'sessions/' + session_id + '/',
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

        # Test for formated session id
        result = requests.post(APIv2 + 'sessions/',
                               data={"url": 'http://example.com', "agent_id": self.agent_new.id,
                                     "format_session_id": True},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        res_json = result.json()
        self.assertNotIn(res_json["id"], res_json["viewer_link"])
        self.assertRegexpMatches(res_json["viewer_link"], r"(\d{3}\-\d{3}\-\d{3}|\d{9})\/?$")


class RESTAPI_v2_agents(TransactionTestCase):

    def setUp(self):
        self.agent = AgentFactory()
        self.apikey = APIKeyFactory(user=self.agent.user, company=self.agent.company)
        self.reseller = ReSellerFactory(user=self.agent.user)
        self.auth = (self.apikey.apikey, '')
        self.widgetauth = (self.apikey.widget_key, '')
        self.agent_id = str(self.agent.id)

    def _check_agents(self, agent):
        self.assertTrue('agent_id' in agent)
        self.assertTrue('agent_name' in agent)
        self.assertTrue('agent_email' in agent)
        self.assertFalse('id' in agent)

    def test_create_agent(self):
        result = requests.post(APIv2 + 'agents/',
                               data={"username": 'helloworld', "agent_email": 'test@surfly.com',
                                     "password": 'example'},
                               auth=self.auth)
        res = result.json()
        self.agent_id = str(res['agent_id'])
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

    def test_create_agent_error_mail(self):
        result = requests.post(APIv2 + 'agents/',
                               data={"username": 'helloworld', "agent_email": 'testmail@surfly.com',
                                     "password": 'example'},
                               auth=self.auth)
        res = result.json()
        self.assertEqual(result.status_code, 200)

        # Test Case : Email Already Exist
        result = requests.post(APIv2 + 'agents/',
                               data={"username": 'helloworld', "agent_email": 'testmail@surfly.com',
                                     "password": 'example'},
                               auth=self.auth)
        res = result.json()
        self.assertEqual(result.status_code, 400)
        self.assertEqual('A user with that email already exists.', res['error'])

        result = requests.post(APIv2 + 'agents/',
                               data={"username": 'helloworld', "agent_email": ' testmail@surfly.com ',
                                     "password": 'example'},
                               auth=self.auth)
        res = result.json()
        self.assertEqual(result.status_code, 400)
        self.assertEqual('A user with that email already exists.', res['error'])

        result = requests.post(APIv2 + 'agents/',
                               data={"username": 'helloworld', "agent_email": 'TestMail@Surfly.Com',
                                     "password": 'example'},
                               auth=self.auth)
        res = result.json()
        self.assertEqual(result.status_code, 400)
        self.assertEqual('A user with that email already exists.', res['error'])

        result = requests.post(APIv2 + 'agents/',
                               data={"username": 'helloworld', "agent_email": 'TestMail@0815.ru',
                                     "password": 'example'},
                               auth=self.auth)
        res = result.json()
        self.assertEqual(result.status_code, 400)
        self.assertEqual('Email service provider is not a real mail service provider!', res['error'])

        result = requests.post(APIv2 + 'agents/',
                               data={"username": 'helloworld',
                                     "agent_email": 'testtesttesttesttesttesttesttesttesttesttesttesttesttesttesttestt@surfly.com',
                                     "password": 'example'},
                               auth=self.auth)
        res = result.json()
        self.assertEqual(result.status_code, 400)
        self.assertEqual('Email address is too long (Maximum of 75 characters)', res['error'])

        result = requests.post(APIv2 + 'agents/',
                               data={"username": 'helloworld', "agent_email": 'test mail @ surfly.com',
                                     "password": 'example'},
                               auth=self.auth)
        res = result.json()
        self.assertEqual(result.status_code, 400)
        self.assertEqual('Not a valid email address', res['error'])

        result = requests.post(APIv2 + 'agents/',
                               data={"username": 'helloworld', "agent_email": 'testmail11@surfly.com',
                                     "password": 'example'},
                               auth=self.auth)
        res = result.json()
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

    def test_check_agent_list(self):
        result = requests.get(APIv2 + 'agents/',
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        agent_list = result.json()
        for agent in agent_list:
            self._check_agents(agent)

    def test_check_agent(self):
        result = requests.get(APIv2 + 'agents/' + self.agent_id + "/",
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        agent = result.json()
        self._check_agents(agent)

    def test_agent_update(self):
        result = requests.put(APIv2 + 'agents/' + self.agent_id + "/",
                              data={"username": 'helloworld1', "agent_email": 'test1@surfly.com'},
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        agent = result.json()
        self.assertEqual(agent['agent_name'], 'helloworld1')
        self.assertEqual(agent['agent_email'], 'test1@surfly.com')

    def test_agent_delete(self):
        self.test_create_agent()
        result = requests.delete(APIv2 + 'agents/' + self.agent_id + "/",
                                 auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

        result = requests.get(APIv2 + 'agents/' + self.agent_id + "/",
                              auth=self.auth)
        self.assertEqual(result.status_code, 404)
        self.assertEqual(result.headers['content-type'], 'application/json')


class RESTAPI_v2_queue(TransactionTestCase):

    def setUp(self):
        self.agent = AgentFactory()
        self.apikey = APIKeyFactory(user=self.agent.user, company=self.agent.company)
        self.reseller = ReSellerFactory(user=self.agent.user)
        self.session = UsageHistoryFactory(company=self.agent.company)
        self.session_id = self.session.session_id
        self.queue = IncomingQueueFactory(session=self.session)
        self.auth = (self.apikey.apikey, '')
        self.widgetauth = (self.apikey.widget_key, '')

    def _check_queue(self, queue):
        self.assertTrue('session' in queue)
        self.assertTrue('url' in queue)
        self.assertTrue('start_time' in queue)

    def test_create_queue(self):
        result = requests.post(APIv2 + 'queue/',
                               data={"url": 'http://google.com/', "id": self.session_id},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        queue_id = result.json()['session']
        self.assertEqual(queue_id, self.session_id)
        result2 = requests.get(APIv2 + 'queue/' + queue_id + "/", auth=self.auth)
        queue_id2 = result2.json()['session']
        self.assertEqual(queue_id, queue_id2)

    def test_create_queue_expire(self):
        result = requests.post(APIv2 + 'queue/',
                               data={"url": 'http://google.com/', "id": self.session_id},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        json_obj = result.json()
        self.session.end_time = datetime.now(pytz.utc)
        self.session.save()
        time.sleep(2)
        queue_id = json_obj['session'].strip()
        result = requests.get(APIv2 + 'queue/' + queue_id + "/", auth=self.auth)
        self.assertEqual(result.status_code, 404)

    def test_create_queue_url_error(self):
        result = requests.post(APIv2 + 'queue/',
                               data={"url": 'google', "id": '1'},
                               auth=self.auth)
        self.assertEqual(result.status_code, 400)
        res = result.json()
        self.assertEqual('Invalid URL!', res['error'])
        self.assertEqual(result.headers['content-type'], 'application/json')

    def test_create_queue_missing_url_error(self):
        result = requests.post(APIv2 + 'queue/',
                               data={"url": ''},
                               auth=self.auth)
        self.assertEqual(result.status_code, 400)
        res = result.json()
        self.assertEqual('missing url', res['error'])

    def test_check_queue_list(self):
        result = requests.get(APIv2 + 'queue/',
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        queue_list = result.json()
        for queue in queue_list:
            self._check_queue(queue)

    def test_check_queue(self):
        result = requests.get(APIv2 + 'queue/' + self.session_id + "/",
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        queue = result.json()
        self._check_queue(queue)

        # Get session by Queue Identifier
        result = requests.get(APIv2 + 'sessions/' + str(self.queue.identifier) + '/',
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        self.assertEqual(result.json().get("viewer_link"), self.session.viewer_link)

    def test_update_queue(self):
        result = requests.put(APIv2 + 'queue/' + self.session_id + "/",
                              data={"join_link": 'http://surfly.com/', "session_id": '2'},
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)

    def test_queue_delete(self):
        result = requests.delete(APIv2 + 'queue/' + self.session_id + "/",
                                 auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

        result = requests.get(APIv2 + 'queue/' + self.session_id + "/",
                              auth=self.auth)
        self.assertEqual(result.status_code, 404)
        self.assertEqual(result.headers['content-type'], 'application/json')


class RESTAPI_v2_client(TransactionTestCase):

    def setUp(self):
        self.agent = AgentFactory()
        self.apikey = APIKeyFactory(user=self.agent.user, company=self.agent.company)
        self.reseller = ReSellerFactory(user=self.agent.user, company=(self.agent.company,))
        PlanPricingFactory(name='reseller_plan', limitations=json.dumps({'reseller_plan': True}))
        self.auth = (self.apikey.apikey, '')
        self.widgetauth = (self.apikey.widget_key, '')
        self.client_key = self.agent.company.client_key

    def _check_client(self, client):
        self.assertTrue('company_name' in client)
        self.assertTrue('client_key' in client)
        self.assertTrue('agent_count' in client)
        self.assertTrue('widget_key' in client)
        self.assertTrue('rest_key' in client)
        self.assertTrue('domains' in client)

    def test_create_client(self):
        company_name = 'Surfly'
        result = requests.post(APIv2 + 'clients/' + self.reseller.reseller_key + '/',
                               data={'company_name': company_name},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        res = result.json()

        # Check the session creation after created client
        result = requests.post(APIv2 + 'sessions/',
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

        # test_create_client_exist_400_error
        result = requests.post(APIv2 + 'clients/' + self.reseller.reseller_key + '/',
                               data={'company_name': company_name},
                               auth=self.auth)
        self.assertEqual(result.status_code, 400)
        res = result.json()
        self.assertEqual('Client already exists!', res['error'])
        self.assertEqual(result.headers['content-type'], 'application/json')

    def test_create_client_number(self):
        result = requests.post(APIv2 + 'clients/' + self.reseller.reseller_key + '/',
                               data={'company_name': 1},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

    def test_create_client_invalid_400_error(self):
        result = requests.post(APIv2 + 'clients/' + self.reseller.reseller_key + '/',
                               data={'company_name': ""},
                               auth=self.auth)
        self.assertEqual(result.status_code, 400)
        res = result.json()
        self.assertEqual('Invalid Input!', res['error'])
        self.assertEqual(result.headers['content-type'], 'application/json')

    def test_client_list(self):
        result = requests.get(APIv2 + 'clients/' + self.reseller.reseller_key + '/',
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        client_list = result.json()
        for client in client_list:
            self._check_client(client)

    def test_get_client(self):
        result = requests.get(APIv2 + 'clients/' + self.reseller.reseller_key + '/' + self.client_key + '/',
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        client = result.json()
        self._check_client(client)

    def test_post_domain_client(self):
        result = requests.post(APIv2 + 'clients/' + self.reseller.reseller_key + '/' + self.client_key + '/',
                               data={'domains': "newdomain.com"},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        res = result.json()
        self.assertEqual(res["domains"], "newdomain.com")

    def test_get_client_error(self):
        result = requests.get(APIv2 + 'clients/' + self.reseller.reseller_key + '/235234263245435425/',
                              auth=self.auth)
        self.assertEqual(result.status_code, 404)
        self.assertEqual(result.headers['content-type'], 'application/json')

    def test_update_client(self):
        result = requests.put(APIv2 + 'clients/' + self.reseller.reseller_key + '/' + self.client_key + '/',
                              data={'company_name': "Surfly Company"},
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        res = result.json()
        self.assertEqual(res['company_name'], 'Surfly Company')
        self.assertEqual(result.headers['content-type'], 'application/json')

    def test_delete_client(self):
        result = requests.delete(APIv2 + 'clients/' + self.reseller.reseller_key + '/' + self.client_key + '/',
                                 auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

        result = requests.get(APIv2 + 'clients/' + self.reseller.reseller_key + '/' + self.client_key + '/',
                              auth=self.auth)
        self.assertEqual(result.status_code, 403)
        self.assertEqual(result.headers['content-type'], 'application/json')


class RESTAPI_v2_optional_clientkey(TransactionTestCase):

    def setUp(self):
        self.agent = AgentFactory()
        self.apikey = APIKeyFactory(user=self.agent.user, company=self.agent.company)
        self.reseller = ReSellerFactory(user=self.agent.user, company=(self.agent.company,))
        self.session = UsageHistoryFactory(company=self.agent.company)
        self.session_id = self.session.session_id
        self.queue = IncomingQueueFactory(session=self.session)
        self.auth = (self.apikey.apikey, '')
        self.widgetauth = (self.apikey.widget_key, '')

    def _check_agents(self, agent):
        self.assertTrue('agent_id' in agent)
        self.assertTrue('agent_name' in agent)
        self.assertTrue('agent_email' in agent)
        self.assertFalse('id' in agent)

    def _check_queue(self, queue):
        self.assertTrue('session' in queue)
        self.assertTrue('start_time' in queue)
        self.assertTrue('url' in queue)

    def test_create_agent(self):
        result = requests.post(APIv2 + 'agents/',
                               data={"username": 'helloworld', "agent_email": 'test@surfly.com',
                                     "password": 'example', "clientkey": self.agent.company.client_key},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

    def test_check_agent_list(self):
        result = requests.get(APIv2 + 'agents/',
                              data={"clientkey": self.agent.company.client_key},
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        agent_list = result.json()
        for agent in agent_list:
            self._check_agents(agent)

    def test_create_session_optional_clientkey(self):
        result = requests.post(APIv2 + 'sessions/?api_key=' + self.apikey.apikey + '&clientkey=' + self.agent.company.client_key,
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)

    def test_create_session_clientkey_data(self):
        result = requests.post(APIv2 + 'sessions/?api_key=' + self.apikey.apikey,
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id,
                                     "clientkey": self.agent.company.client_key})
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)

    def test_check_queue_list_clientkey(self):
        result = requests.get(APIv2 + 'queue/',
                              data={"clientkey": self.agent.company.client_key},
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        queue_list = result.json()
        for queue in queue_list:
            self._check_queue(queue)


class WIDGETAPI_v2_Test(TransactionTestCase):

    def setUp(self):
        self.company = CompanyFactory(domains="*.google.com, google.com")
        self.agent = AgentFactory(company=self.company)
        self.apikey = APIKeyFactory(user=self.agent.user, company=self.agent.company)
        self.reseller = ReSellerFactory(user=self.agent.user)
        self.auth = (self.apikey.apikey, '')
        self.widgetauth = (self.apikey.widget_key, '')

    def test_cors_forbidden_request(self):
        result = requests.post(APIv2 + 'clients/' + self.agent.company.client_key + '/',
                               headers={"origin": "http://google.com"},
                               auth=self.widgetauth)
        self.assertEqual(result.status_code, 403)
        self.assertEqual(result.json(), {u'detail': u'Your WIDGET key cannot be used for this type of request.'})

    def test_cors_request_apikey(self):
        result = requests.post(APIv2 + 'sessions/?api_key=' + self.apikey.widget_key,
                               headers={"origin": "http://www.google.com"},
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 200)
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)

    def test_cors_request_auth(self):
        result = requests.post(APIv2 + 'sessions/',
                               headers={"origin": "http://google.com/somepage.html"},
                               auth=self.widgetauth,
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 200)
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)

    def test_cors_request_invalid_domain(self):
        result = requests.post(APIv2 + 'sessions/',
                               headers={"origin": "https://www.google.com"},
                               auth=self.widgetauth,
                               data={"url": 'http://www.facebook.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 403)
        self.assertEqual(result.json(), {u'error': u'Domain is not allowed by API key'})

    def test_cors_request_missing_origin(self):
        result = requests.post(APIv2 + 'sessions/',
                               auth=self.widgetauth,
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 403)
        self.assertEqual(result.json(), {u'detail': u'Your request is missing an origin header - maybe you should use your REST key?'})

    def test_cors_request_invalid_origin(self):
        result = requests.post(APIv2 + 'sessions/',
                               headers={"origin": "www.facebook.com"},
                               auth=self.widgetauth,
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 403)
        self.assertEqual(result.json(), {u'detail': u'Your origin domain is not listed on your Surfly integration page.'})

    def test_cors_request_auth_clientkey_optional(self):
        result = requests.post(APIv2 + 'sessions/?clientkey=' + self.agent.company.client_key,
                               headers={"origin": "www.google.com"},
                               auth=self.widgetauth,
                               data={"url": 'http://www.google.com', "agent_id": self.agent.id})
        self.assertEqual(result.status_code, 200)
        json_obj = result.json()
        self.assertTrue('leader_link' in json_obj)
        self.assertTrue('viewer_link' in json_obj)
        self.assertTrue('id' in json_obj)


class WebhookTest(TransactionTestCase):
    """
    This webhook test are for those webhooks which we call
    """

    def setUp(self):
        self.agent = AgentFactory()
        self.agent.company.stripe_customer_id = "cus_00000000000000"
        self.agent.company.save()
        self.referrer_history = ReferrerHistoryFactory(user=self.agent.user)
        self.referrer_history.registered_company.add(self.agent.company)
        self.auth = (self.agent.user.email, "example")
        self.headers = {'content-type': 'application/json'}
        self.payload = {
            "type": "charge.succeeded",
            "data": {"object": {"customer": "cus_00000000000000"}},
        }

    def test_charge_succeed_200(self):
        result = requests.post(SITE + "charge/succeed/webhook/",
                               headers=self.headers,
                               data=json.dumps(self.payload),
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)

    def test_charge_succeed_404(self):
        self.payload["type"] = "charge.created"
        result = requests.post(SITE + "charge/succeed/webhook/",
                               headers=self.headers,
                               data=json.dumps(self.payload),
                               auth=self.auth)
        self.assertEqual(result.status_code, 404)

    def test_charge_succeed_500(self):
        self.payload['data']["object"]["customer"] = "cus_123456"
        result = requests.post(SITE + "charge/succeed/webhook/",
                               headers=self.headers,
                               data=json.dumps(self.payload),
                               auth=self.auth)
        self.assertEqual(result.status_code, 500)


class APIDocTest(TransactionTestCase):

    def setUp(self):
        # Can't use Faker in this test case, Because of the below static Keys used in our API Doc
        # http://docs.surfly.apiary.io/
        # Also it requires fixed Agents ids's

        # Reset db sequence
        from django.core.management import call_command
        call_command("sqlsequencereset", "surflyapp")

        company = CompanyFactory(domains="surfly.com, *.surfly.com", client_key='00063668234b4aa19dd436e6a76ab8ae')
        agent = AgentFactory(company=company, id=1)
        # For Deleting Client & Agent APIAPRY Doc purpose, Added the below extra data's
        CompanyFactory(client_key='63668234b4aa19dd436e6a76ab8ae000')
        agent_2 = AgentFactory(company=company, id=2)
        APIKeyFactory(user=agent.user, company=company, apikey='3b57bd2db21c46d2875508b982968d26')
        ReSellerFactory(user=agent.user, reseller_key='02fbfd63b54341c1828e6a2426311788', company=(company,))
        self.session = UsageHistoryFactory(company=company, session_id="fuSHr0sRQ1usugvheahwQ")
        IncomingQueueFactory(session=self.session)
        os.system('wget https://raw.githubusercontent.com/surfly/API/master/apiary.apib -O apiary.apib')
        PlanPricingFactory(name='business')
        PlanPricingFactory(name='reseller_plan', limitations=json.dumps({'reseller_plan': True}))

        description = "Date from 2015-06-29 to 2015-01-29"
        invoice_details = {
            "currency": "euro", "issue_date": "2015-6-06",
            "to_address": "ABC,1221,Netherlands,4343", "from_address": "Surfly Admin, Netherlands",
            "payment_term": 14, "invoice_number": 5, "item_list": [{"amount": 40.0, "description": description, "unit_price": 0.67, "quantity": 60}],
            "total": 1490.0, "sub_total": 1490.0}
        InvoiceFactory(company=company, price=200.0, invoice_details=json.dumps(invoice_details))

    def test_apiary_documentation(self):
        retval = os.system('dredd apiary.apib http://127.0.0.1:8010/v2/')
        self.assertEqual(retval, 0)

    def tearDown(self):
        os.remove('apiary.apib')


from surflyapp.invoice_tests import (UssageInvoiceTest, AdvanceInvoiceTest,
                                     CancelIvoiceTest)

ASSIGN_JUST_TO_PASS_LINT = (UssageInvoiceTest, AdvanceInvoiceTest,
                            CancelIvoiceTest)


class InvoiceAPITest(TransactionTestCase):

    def setUp(self):
        company = CompanyFactory(domains="surfly-dev.com")
        self.agent = AgentFactory(company=company)
        self.apikey = APIKeyFactory(user=self.agent.user, company=company)
        self.reseller = ReSellerFactory(user=self.agent.user, company=(company,))
        self.today = date.today()
        invoice_details = {
            "currency": "euro", "issue_date": "%s-%s-06" % (self.today.year, self.today.month),
            "to_address": "ABC,1221,Netherlands,4343", "from_address": "Admin, Surfly",
            "payment_term": 14, "invoice_number": 5, "item_list": [
                {"amount": 40.0, "description": "Date from 2015-04-06 to 2015-04-07", "unit_price": 0.67, "quantity": 60},
                {"amount": 40.67, "description": "Date from 2015-04-07 to 2015-04-08", "unit_price": 0.67, "quantity": 61},
                {"amount": 41.33, "description": "Date from 2015-04-08 to 2015-04-09", "unit_price": 0.67, "quantity": 62}],
            "total": 1490.0, "sub_total": 1490.0}
        InvoiceFactory(company=company, price=200.0, invoice_details=json.dumps(invoice_details),
                       subscription_startdate=self.today, subscription_enddate=self.today)

        self.created_at = self.today - relativedelta(years=1)
        for month in [04, 05]:
            invoice_details = {
                "currency": "euro", "issue_date": "%s-%s-06" % (self.created_at.year, month),
                "to_address": "ABC,1221,Netherlands,4343", "from_address": "Admin, Surfly",
                "payment_term": 14, "invoice_number": 5, "item_list": [
                    {"amount": 40.0, "description": "Agents details with date range", "unit_price": 0.67, "quantity": 60},
                    {"amount": 40.67, "description": "Agents details with date range", "unit_price": 0.67, "quantity": 61},
                    {"amount": 41.33, "description": "Agents details with date range", "unit_price": 0.67, "quantity": 62}],
                "total": 1490.0, "sub_total": 1490.0}
            invoice = InvoiceFactory(company=company, price=200.0, invoice_details=json.dumps(invoice_details),
                                     subscription_startdate='2014-%s-06' % month, subscription_enddate='2014-%s-06' % month)
            invoice.created = date(self.created_at.year, month, 06)
            invoice.save()

        self.auth = (self.apikey.apikey, '')
        self.widgetauth = (self.apikey.widget_key, '')
        self.agent_id = str(self.agent.id)
        self.reseller_auth = (self.reseller.reseller_key, '')

    def _check_invoice(self, invoice):
        self.assertTrue('total' in invoice)
        self.assertTrue('issue_date' in invoice)
        self.assertTrue('currency' in invoice)
        self.assertTrue('item_list' in invoice)

    def test_get_invoice_details_with_year(self):
        result = requests.post(APIv2 + 'get/invoice/details/',
                               {'year': self.created_at.year},
                               auth=self.auth)

        self.assertEqual(result.status_code, 200)
        invoice_detail_list = result.json()
        self.assertEqual(len(invoice_detail_list), 2)
        for invoice in invoice_detail_list:
            self._check_invoice(invoice)

    def test_get_invoice_details_with_year_month(self):
        result = requests.post(APIv2 + 'get/invoice/details/',
                               {'year': self.created_at.year, 'month': 04},
                               auth=self.auth)

        self.assertEqual(result.status_code, 200)
        invoice_detail_list = result.json()
        self.assertEqual(len(invoice_detail_list), 1)
        for invoice in invoice_detail_list:
            self._check_invoice(invoice)
            self.assertEqual(invoice['issue_date'], "%s-4-06" % self.created_at.year)

    def test_get_invoice_details_current_year(self):
        # Check the current year invoice details
        result = requests.post(APIv2 + 'get/invoice/details/',
                               auth=self.auth)

        self.assertEqual(result.status_code, 200)
        invoice_detail_list = result.json()
        self.assertEqual(len(invoice_detail_list), 1)
        for invoice in invoice_detail_list:
            self._check_invoice(invoice)
            self.assertEqual(invoice['issue_date'], "%s-%s-06" % (self.today.year, self.today.month))

    def test_get_invoice_details_widget_auth(self):
        # Check the current year invoice details using widget auth
        result = requests.post(APIv2 + 'get/invoice/details/',
                               headers={"origin": "surfly-dev.com"},
                               auth=self.widgetauth)
        self.assertEqual(result.status_code, 200)
        invoice_detail_list = result.json()
        self.assertEqual(len(invoice_detail_list), 1)
        for invoice in invoice_detail_list:
            self._check_invoice(invoice)
            self.assertEqual(invoice['issue_date'], "%s-%s-06" % (self.today.year, self.today.month))

    def test_get_invoice_details_reseller_auth(self):
        # Check the current year invoice details using Reseller auth
        result = requests.post(APIv2 + 'get/invoice/details/?clientkey=' + self.agent.company.client_key,
                               auth=self.reseller_auth)
        self.assertEqual(result.status_code, 200)
        invoice_detail_list = result.json()
        self.assertEqual(len(invoice_detail_list), 1)
        for invoice in invoice_detail_list:
            self._check_invoice(invoice)
            self.assertEqual(invoice['issue_date'], "%s-%s-06" % (self.today.year, self.today.month))


class ResellerWebHookTest(TransactionTestCase):
    """
    These webhooks are the webhooks which we provide for reseller to keep track agent/client activity
    """

    def setUp(self):
        self.user = UserFactory()
        self.company = CompanyFactory(agent_size=1)
        self.agent = AgentFactory()
        self.reseller = ReSellerFactory(user=self.user, webhook=SITE + "test/figlo/webhook/")
        self.reseller_account = ResellerCompanyAccountsFactory(reseller=self.reseller, company=self.company)
        self.reseller_account.agent.add(self.agent)
        self.reseller_account.save()
        self.rm = ResellerMutationsFactory(reseller=self.reseller, agent=self.agent)
        self.created = self.rm.time

    def test_figlo_webhook(self):
        update_current_add_to_figlo_webhook(self.reseller)
        excepted_content = {u'account_id': unicode(self.reseller_account.account_id),
                            u'agent_email': u'{0}'.format(self.agent.user.email),
                            u'total_agent_size': 1,
                            u'is_new_agent': True,
                            u'is_new_account': True,
                            u"created": unicode(self.created)}
        updated_rm = ResellerMutations.objects.get(id=self.rm.id)
        webhook_response = json.loads(updated_rm.webhook_response_content)
        self.maxDiff = None
        self.assertDictEqual(excepted_content, webhook_response)
        self.assertEqual(updated_rm.webhook_response_code, 200)
        self.assertEqual(updated_rm.webhook_status, True)

    def test_add_client(self):
        new_add_client = CompanyFactory(agent_size=1)
        log_and_call_add_client_webhook(self.reseller, new_add_client)
        new_client_rm = ResellerMutations.objects.get(reseller=self.reseller, action="add_client",
                                                      company=new_add_client)

        excepted_content = {u'action': u'add_client',
                            u'data': {'client_agent_size': 1,
                                      u'client_id': new_add_client.id,
                                      u'client_key': new_add_client.client_key,
                                      u'client_name': new_add_client.name,
                                      u'client_plan': '',
                                      u'client_rest_key': '',
                                      u'client_widget_key': ''},
                            u'id': new_client_rm.id}

        webhook_response = json.loads(new_client_rm.webhook_response_content)
        webhook_response.pop("timestamp")
        webhook_response["data"].pop("client_created")
        webhook_response["data"].pop("client_last_active")
        self.maxDiff = None
        self.assertDictEqual(excepted_content, webhook_response)
        self.assertEqual(new_client_rm.webhook_response_code, 200)
        self.assertEqual(new_client_rm.webhook_status, True)

    def test_remove_client(self):
        remove_client = CompanyFactory(agent_size=1)
        log_and_call_remove_client_webhook(self.reseller, remove_client)
        remove_client_rm = ResellerMutations.objects.get(reseller=self.reseller, action="remove_client")

        excepted_content = {u'action': u'remove_client',
                            u'id': remove_client_rm.id,
                            u'data': {u'client_id': remove_client.id,
                                      u'client_key': remove_client.client_key,
                                      u'client_name': remove_client.name}}

        webhook_response = json.loads(remove_client_rm.webhook_response_content)
        webhook_response.pop("timestamp")
        webhook_response["data"].pop("client_delete_time")
        self.maxDiff = None
        self.assertDictEqual(excepted_content, webhook_response)
        self.assertEqual(remove_client_rm.webhook_response_code, 200)
        self.assertEqual(remove_client_rm.webhook_status, True)

    def test_add_agent(self):
        client = CompanyFactory(agent_size=1)
        new_user = UserFactory()
        new_agent = AgentFactory(user=new_user, company=client)
        log_and_call_add_agent_webhook(self.reseller, client, new_agent)
        add_agent_rm = ResellerMutations.objects.get(reseller=self.reseller, agent=new_agent,
                                                     company=client, action="add_agent")

        excepted_content = {u'action': u'add_agent',
                            u'id': add_agent_rm.id,
                            u'data': {u'agent_name': unicode(new_user.first_name),
                                       u'agent_email': new_user.email,
                                       u'client_name': client.name,
                                       u'client_agent_size': 1,
                                       u'agent_id': new_agent.id,
                                       u'client_id': client.id,
                                       u'client_key': unicode(client.client_key)}}

        webhook_response = json.loads(add_agent_rm.webhook_response_content)
        webhook_response.pop("timestamp")
        self.maxDiff = None
        self.assertDictEqual(excepted_content, webhook_response)
        self.assertEqual(add_agent_rm.webhook_response_code, 200)
        self.assertEqual(add_agent_rm.webhook_status, True)

    def test_remove_agent(self):
        client = CompanyFactory(agent_size=1)
        old_user = UserFactory(first_name='')
        old_agent = AgentFactory(user=old_user, company=client)
        log_and_call_remove_agent_webhook(self.reseller, client, old_agent)
        remove_agent_rm = ResellerMutations.objects.get(reseller=self.reseller, company=client, action="remove_agent")

        excepted_content = {u'action': u'remove_agent',
                            u'id': remove_agent_rm.id,
                            u'data': {u'agent_name': u'',
                                       u'agent_email': unicode(old_user.email),
                                       u'client_name': client.name,
                                       u'client_agent_size': 1,
                                       u'agent_id': old_agent.id,
                                       u'client_id': client.id,
                                       u'client_key': unicode(client.client_key)}}

        webhook_response = json.loads(remove_agent_rm.webhook_response_content)
        webhook_response.pop("timestamp")
        self.maxDiff = None
        self.assertDictEqual(excepted_content, webhook_response)
        self.assertEqual(remove_agent_rm.webhook_response_code, 200)
        self.assertEqual(remove_agent_rm.webhook_status, True)

    def test_agent_size_change(self):
        client = CompanyFactory(agent_size=1)

        def agent_size_increase():
            action = "agent_size_increase"
            client.agent_size += 2
            client.save()
            log_and_call_custom_action_webhook(self.reseller, client, action)
            agent_size_increase_rm = ResellerMutations.objects.get(reseller=self.reseller, company=client, action="agent_size_increase")
            excepted_content = {u'action': u'agent_size_increase',
                                u'data': {u'client_agent_size': 3,
                                          u'client_id': client.id,
                                          u'client_key': client.client_key,
                                          u'client_name': client.name},
                                u'id': agent_size_increase_rm.id}

            webhook_response = json.loads(agent_size_increase_rm.webhook_response_content)
            webhook_response.pop("timestamp")
            self.maxDiff = None
            self.assertDictEqual(excepted_content, webhook_response)
            self.assertEqual(agent_size_increase_rm.webhook_response_code, 200)
            self.assertEqual(agent_size_increase_rm.webhook_status, True)

        def agent_size_decrease():
            action = "agent_size_decrease"
            client.agent_size -= 1
            client.save()
            log_and_call_custom_action_webhook(self.reseller, client, action)
            agent_size_decrease_rm = ResellerMutations.objects.get(reseller=self.reseller, company=client, action="agent_size_decrease")
            excepted_content = {u'action': u'agent_size_decrease',
                                u'data': {u'client_agent_size': 2,
                                          u'client_id': client.id,
                                          u'client_key': client.client_key,
                                          u'client_name': client.name},
                                u'id': agent_size_decrease_rm.id}

            webhook_response = json.loads(agent_size_decrease_rm.webhook_response_content)
            webhook_response.pop("timestamp")
            self.maxDiff = None
            self.assertDictEqual(excepted_content, webhook_response)
            self.assertEqual(agent_size_decrease_rm.webhook_response_code, 200)
            self.assertEqual(agent_size_decrease_rm.webhook_status, True)

        agent_size_increase()
        agent_size_decrease()

    def test_subscription_change(self):
        client = CompanyFactory(agent_size=1)
        user = UserFactory()
        AgentFactory(user=user, company=client)
        api_key = APIKeyFactory(company=client, user=user)

        def upgrade():
            action = "upgrade_subscription"
            log_and_call_custom_action_webhook(self.reseller, client, action)
            upgrade_subscription_rm = ResellerMutations.objects.get(reseller=self.reseller,
                                                                    company=client, action="upgrade_subscription")

            excepted_content = {u'action': u'upgrade_subscription',
                                u'id': upgrade_subscription_rm.id,
                                u'data': {u'client_agent_size': client.agent_size,
                                          u'client_id': client.id,
                                          u'client_key': client.client_key,
                                          u'client_name': client.name,
                                          u'client_plan': u'',
                                          u'client_rest_key': api_key.apikey,
                                          u'client_widget_key': u''}}

            webhook_response = json.loads(upgrade_subscription_rm.webhook_response_content)
            webhook_response.pop("timestamp")
            self.maxDiff = None
            self.assertDictEqual(excepted_content, webhook_response)
            self.assertEqual(upgrade_subscription_rm.webhook_response_code, 200)
            self.assertEqual(upgrade_subscription_rm.webhook_status, True)

        def downgrade():
            api_key.enabled = False
            api_key.save()
            action = "downgrade_subscription"
            log_and_call_custom_action_webhook(self.reseller, client, action)
            downgrade_subscription_rm = ResellerMutations.objects.get(reseller=self.reseller,
                                                                      company=client, action="downgrade_subscription")

            excepted_content = {u'action': u'downgrade_subscription',
                                u'id': downgrade_subscription_rm.id,
                                u'data': {u'client_agent_size': client.agent_size,
                                          u'client_id': client.id,
                                          u'client_key': client.client_key,
                                          u'client_name': client.name,
                                          u'client_plan': u'',
                                          u'client_rest_key': u'',
                                          u'client_widget_key': u''}}

            webhook_response = json.loads(downgrade_subscription_rm.webhook_response_content)
            webhook_response.pop("timestamp")
            self.maxDiff = None
            self.assertDictEqual(excepted_content, webhook_response)
            self.assertEqual(downgrade_subscription_rm.webhook_response_code, 200)
            self.assertEqual(downgrade_subscription_rm.webhook_status, True)

        upgrade()
        downgrade()


class UtilFunTest(TransactionTestCase):

    def setUp(self):
        self.user = UserFactory()
        self.user1 = UserFactory()
        self.stripe_plan = PlanPricingFactory()
        self.advance_invoice_plan = PlanPricingFactory(name='advance_invoice', payment_method="advance_invoice")
        self.usage_invoice_plan = PlanPricingFactory(name='usage_invoice', payment_method="usage_invoice")
        self.company = CompanyFactory()
        self.company1 = CompanyFactory(pricing_plan=self.advance_invoice_plan)
        self.company2 = CompanyFactory(pricing_plan=self.advance_invoice_plan)
        BillingInfoFactory(company=self.company2, zip_address="1018", country='NL', vatid="NL851751271B01")
        AgentFactory(user=self.user, company=self.company2)
        AgentFactory(user=self.user1, company=self.company2)
        self.company3 = CompanyFactory(pricing_plan=self.usage_invoice_plan)
        # For Testing Purpose Assigned wrong country code 'US' -> 'USA'
        BillingInfoFactory(company=self.company3, zip_address="987654321", country='USAWRONG')

    def test_is_invoice_client(self):
        self.assertEqual(is_invoice_client(self.company), False)
        self.assertEqual(is_invoice_client(self.company1), True)
        self.assertEqual(is_invoice_client(self.company2), True)
        self.assertEqual(is_invoice_client(self.company3), True)

    def test_create_or_update_quaderno_contact(self):
        # Company without Billing Address
        res = create_or_update_quaderno_contact(self.company1)
        quaderno_trans1 = QuadernoTransaction.objects.get(company=self.company1)
        self.assertEqual(res, int(quaderno_trans1.contact_id))

        # Company with Billing Address
        res = create_or_update_quaderno_contact(self.company2)
        # Check the Admin Mail IDs, user & user1 are Admin
        self.assertEqual(self.company2.admin_email(), u'{0},{1}'.format(self.user.email, self.user1.email))
        quaderno_trans = QuadernoTransaction.objects.get(company=self.company2)
        self.assertEqual(res, int(quaderno_trans.contact_id))


class RESTAPI_v2_Reseller_Authentication(TransactionTestCase):

    def setUp(self):
        self.agent = AgentFactory()
        self.agent1 = AgentFactory()
        self.apikey = APIKeyFactory(user=self.agent.user, company=self.agent.company)
        self.reseller = ReSellerFactory(user=self.agent.user, company=(self.agent.company,))
        PlanPricingFactory()
        PlanPricingFactory(name='business')
        PlanPricingFactory(name='reseller_plan', limitations={'reseller_plan': True})
        self.auth = (self.reseller.reseller_key, '')
        self.invalid_auth = (self.reseller.reseller_key + '1', '')

    def test_create_client_using_reseller_key(self):
        result = requests.post(APIv2 + 'clients/' + self.reseller.reseller_key + '/',
                               data={'company_name': 'Surfly'},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        res = result.json()
        self.assertEqual(res['company_name'], 'Surfly')

    def test_create_agent_using_reseller_key(self):
        result = requests.post(APIv2 + 'agents/?clientkey=' + self.agent.company.client_key,
                               data={"username": 'helloworld', "agent_email": 'test@surfly.com',
                                     "password": 'example'},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        res = result.json()
        agent = Agent.objects.get(id=res['agent_id'])
        self.assertEqual(agent.user.first_name, "helloworld")
        self.assertEqual(agent.user.email, "test@surfly.com")

        result = requests.get(APIv2 + 'agents/?api_key={0}&clientkey={1}'.format(self.reseller.reseller_key, self.agent.company.client_key))
        self.assertEqual(result.status_code, 200)
        res = result.json()
        self.assertEqual(len(res), 2)

    def test_check_queue_using_reseller_key(self):
        self.session = UsageHistoryFactory(company=self.agent.company)
        self.queue = IncomingQueueFactory(session=self.session)

        result = requests.get(APIv2 + 'queue/',
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        res = result.json()
        self.assertEqual(len(res), 1)

    def test_create_session_using_reseller_key(self):
        result = requests.get(APIv2 + 'sessions/',
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

        result = requests.post(APIv2 + 'sessions/?clientkey=' + self.agent.company.client_key,
                               data={"url": 'http://www.google.com'},
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')

    def test_invalid_auth(self):
        result = requests.post(APIv2 + 'clients/' + self.reseller.reseller_key + '/',
                               data={'company_name': 'Surfly_New'},
                               auth=self.invalid_auth)
        self.assertEqual(result.status_code, 403)
        res = result.json()
        self.assertEqual(res['detail'], 'Invalid API key!')

        result = requests.get(APIv2 + 'clients/{0}/?api_key={1}{2}'.format(self.reseller.reseller_key, self.reseller.reseller_key, 1))
        self.assertEqual(result.status_code, 403)
        res = result.json()
        self.assertEqual(res['detail'], 'Invalid API key!')

        result = requests.get(APIv2 + 'agents/?api_key={0}&clientkey={1}'.format(self.reseller.reseller_key, self.agent1.company.client_key))
        self.assertEqual(result.status_code, 403)
        res = result.json()
        self.assertEqual(res['detail'], u'You are not a reseller for the requested company.')


class IncomingQueueTest(TransactionTestCase):

    def setUp(self):
        self.agent = AgentFactory()
        self.user1 = UserFactory()
        self.apikey = APIKeyFactory(user=self.agent.user, company=self.agent.company)
        self.reseller = ReSellerFactory(user=self.agent.user, company=(self.agent.company,))
        self.plan = PlanPricingFactory()
        PlanPricingFactory(name='business')
        self.auth = (self.apikey.apikey, '')
        self.client = Client()

    def check_options(self, link):
        page = requests.get(link)
        if page.status_code == 200:
            # After installing the html5lib BS4's find is not working
            # So html.pareser is added
            soup = BeautifulSoup(page.text, 'html.parser')
            res_chat = soup.find(id="chat_b4_dock")

            if res_chat:
                return True
        return False

    def test_create_queue(self):
        res_session = requests.post(APIv2 + 'sessions/',
                                    data={"url": 'http://www.google.com', "agent_id": self.agent.id},
                                    auth=self.auth)
        self.assertEqual(res_session.status_code, 200)
        session_id = res_session.json()['id']
        # Leader link opned, To avoid the follower link opened before leader link problem
        self.check_options(res_session.json()['leader_link'])

        res_queue = requests.post(APIv2 + 'queue/',
                                  data={"url": 'http://google.com/', "id": session_id},
                                  auth=self.auth)
        self.assertEqual(res_queue.status_code, 200)
        session_id = res_queue.json()['session']

        response = self.client.post(reverse('incoming_queue'), {'session_id': session_id})
        # Redirecting to the login page
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.endswith('/login/?next=/incoming_queue/'))

        # Using client method because of @login_required
        self.client.login(username=self.agent.user.username, password=USER_PASSWORD)
        # Post the session_id to incoming_queue()
        response = self.client.post(reverse('incoming_queue'), {'session_id': session_id})
        self.assertEqual(response.status_code, 302)
        # Check the follower link (Incoming Queue page's take call)
        self.assertTrue(self.check_options(response['Location']))

        # test_user1 without agent object
        self.client.login(username=self.user1.username, password=USER_PASSWORD)
        # Post the session_id to incoming_queue()
        response = self.client.post(reverse('incoming_queue'), {'session_id': session_id})
        self.assertEqual(response.status_code, 302)
        self.assertTrue(response.url.endswith('/'))


class MeetingApiTest(TransactionTestCase):
    def setUp(self):
        self.plan = PlanPricingFactory(limitations=json.dumps({"meeting": True}))
        self.agent = AgentFactory()
        self.agent.company.pricing_plan = self.plan
        self.agent.company.save()
        self.reg_profile = RegistrationProfileFactory(user=self.agent.user, activation_key='ALREADY_ACTIVATED')
        self.apikey = APIKeyFactory(user=self.agent.user, company=self.agent.company)
        self.auth = (self.apikey.apikey, '')
        self.maxdate = datetime(2030, 1, 1, 00, 00, 00, 000000, pytz.timezone('UTC'))

        self.create_data = {"subject": ' '.join(faker.words()),
                            "start_time": self.create_datetimestamp(ip_timezone="Europe/Amsterdam"),
                            "duration": faker.random_int(max=600),
                            "participants": ','.join([faker.email() for i in range(3)])
                            }

    @staticmethod
    def create_datetimestamp(start=datetime.now(tz=pytz.timezone('UTC')),
                             end=datetime.now(tz=pytz.timezone('UTC')), ip_timezone="UTC"):
        start = convert_datetime_by_timezone(start, to_tz_name=ip_timezone)
        end = convert_datetime_by_timezone(end, to_tz_name=ip_timezone)
        return str(faker.date_time_between(start, end).replace(tzinfo=pytz.timezone(ip_timezone))).replace(' ', "T")

    def _assert_meeting(self, res):
        self.assertEqual(res["subject"], self.create_data["subject"])
        self.assertEqual(res["duration"], self.create_data["duration"])
        self.assertEqual(set(res["participant_link"].keys()), set(self.create_data['participants'].split(",")))
        requested_data = parse(self.create_data["start_time"])
        requested_data = convert_datetime_by_timezone(requested_data, self.reg_profile.time_zone)
        res_date = parse(res["start"])
        self.assertEqual(requested_data, res_date)

    def test_create_update_meeting(self):

        # Try with past datetime
        self.create_data["start_time"] = self.create_datetimestamp(datetime.now(tz=pytz.timezone('UTC')) - timedelta(2),
                                                                   datetime.now(tz=pytz.timezone('UTC')) - timedelta(1))
        result = requests.post(APIv2 + 'meeting/',
                               data=self.create_data,
                               auth=self.auth)
        self.assertEqual(result.status_code, 400)
        self.assertEqual(result.headers['content-type'], 'application/json')
        exp_res = {u'error': {u'start_time': [u'Meeting start time already passed. Please create a meeting in future time.']}}
        self.assertDictEqual(result.json(), exp_res)

        # Try with current datetime
        self.create_data["start_time"] = self.create_datetimestamp()
        result = requests.post(APIv2 + 'meeting/',
                               data=self.create_data,
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        res = result.json()
        self._assert_meeting(res)

        self.create_data["start_time"] = self.create_datetimestamp(end=self.maxdate, ip_timezone="Asia/Calcutta")
        result = requests.post(APIv2 + 'meeting/',
                               data=self.create_data,
                               auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        self.create_res = result.json()
        self._assert_meeting(self.create_res)

        # Try update meeting
        self.create_data["participants"] = ','.join([faker.email() for i in range(3)])
        self.create_data.update({"meeting_key": self.create_res['meeting_key']})
        result = requests.put(APIv2 + 'meeting/',
                              data=self.create_data,
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        update_res = result.json()
        self._assert_meeting(update_res)

        # Test get one meeting result
        get_data = {"meeting_key": update_res['meeting_key']}
        result = requests.get(APIv2 + 'meeting/',
                              data=get_data,
                              auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        self.maxDiff = None
        self.assertDictEqual(result.json(), update_res)

        # Test delete meeting
        delete_data = {"meeting_key": update_res['meeting_key']}
        result = requests.delete(APIv2 + 'meeting/',
                                 data=delete_data,
                                 auth=self.auth)
        self.assertEqual(result.status_code, 200)
        self.assertEqual(result.headers['content-type'], 'application/json')
        self.maxDiff = None
        exp_res = {u'response': u'Meeting deleted Successfully'}
        self.assertDictEqual(result.json(), exp_res)


class MeetingByMailTest(TransactionTestCase):
    def setUp(self):
        self.agent = AgentFactory(session_quota_monthly=1, session_quota_pack=1)
        self.agent1 = AgentFactory(session_quota_pack=1)
        RegistrationProfileFactory(user=self.agent.user, activation_key='ALREADY_ACTIVATED')

        self.plan = PlanPricingFactory(limitations=json.dumps({"meeting": True}))
        self.client = Client()

        # Sets the user's password
        self.username = self.agent.user.username
        self.agent.company.pricing_plan = self.plan
        self.agent.company.save()
        with open('test_ical_post_data.json') as fp:
            self.post_data = json.loads(fp.read())
        temp_data = json.loads(self.post_data.get('mandrill_events')[0])
        temp_data[0]["msg"]["from_email"] = self.agent.user.email
        self.post_data = {"mandrill_events": [json.dumps(temp_data)]}

    def test_create_meeting_by_mail(self):
        self.client.post('/login/', {'username': self.username, 'password': USER_PASSWORD})

        result = self.client.post(SITE + "meeting/bymail/", self.post_data)
        self.assertEqual(result.status_code, 200)
        self.assertIn("Meeting Successfully created", result.content)

        # Change the DSTART Date as expire date
        data = self.update_ical_value(**{"dtstart": vDatetime(datetime.now() - timedelta(0, 1800))})
        result = self.client.post(SITE + "meeting/bymail/", data)
        self.assertEqual(result.status_code, 200)
        self.assertIn("The meeting time already passed", result.content)

        # Create meeting for current time
        data = self.update_ical_value(**{"dtstart": vDatetime(datetime.now())})
        result = self.client.post(SITE + "meeting/bymail/", data)
        self.assertEqual(result.status_code, 200)
        self.assertIn("Meeting Successfully created", result.content)

        # Create New Meeting - Update UID
        curr_uid = "74q3gd9m60p3ib9lc8rj0b9k6dj30b9p6gp64bb5cco62p1hcgo6cor474@google.com"
        data = self.update_ical_value(**{"uid": vText(curr_uid[5:])})
        result = self.client.post(SITE + "meeting/bymail/", data)
        self.assertEqual(result.status_code, 200)
        self.assertIn("Meeting Successfully created", result.content)

    def update_ical_value(self, **kwargs):
        temp_data = json.loads(self.post_data.get('mandrill_events')[0])
        i_content = base64.b64decode(temp_data[0]["msg"]["attachments"]["invite.ics"]["content"])
        gcal = Calendar.from_ical(i_content)
        for component in gcal.walk():
            if not component.name == "VEVENT":
                continue
            for key, value in kwargs.items():
                component[key] = value
        temp_data[0]["msg"]["attachments"]["invite.ics"]["content"] = base64.b64encode(Calendar.to_ical(gcal))
        return {"mandrill_events": [json.dumps(temp_data)]}


def _kill(popen):
    if popen.poll() is not None:
        return
    try:
        popen.send_signal(1)
    except OSError:
        return
    time.sleep(1)
    if popen.poll() is not None:
        return
    try:
        popen.kill()
    except OSError:
        pass

