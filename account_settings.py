#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import logging
import re
import requests
import uuid
from datetime import date
from string import Template

import analytics
import stripe
import GeoIP
from gevent import spawn

from django.contrib.auth.models import User
from django.template import RequestContext
from django.shortcuts import render
from django.template.loader import render_to_string
from django.contrib.auth.decorators import login_required
from django.views.decorators.http import require_POST
from django.core.urlresolvers import reverse
from django.core.exceptions import FieldError
from django.contrib import messages
from django.db import IntegrityError
from django.db.models import Max, Min

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse, HttpResponseRedirect

from registration.models import RegistrationProfile

from django.conf import settings
from surflyapp.util import (is_user_activated,
                            activate_account, hash_email,
                            is_client_type_of, is_invoice_client, get_client_ip,
                            log_and_call_custom_action_webhook,
                            get_quaderno_tax_rate,
                            create_or_update_quaderno_contact,
                            get_time_zone_by_ip,
                            update_free_conf_minute,
                            update_conf_minutes,
                            format_session_id,
                            get_site,
                            get_set_quaderno_contact_id_for_stripe_user)
from registration.signals import user_registered
from surflyapp.decorators import is_company_admin, memoize
from surflyapp.mailchimp import SubscribeJob
from surflyapp.custom_mail import custom_send_mail
from surflyapp.urlcheck import ValidURL, InvalidURL
from surflyapp.agents import can_change_admin

from surflyapp.models import (BillingInfo, QuadernoTransaction,
                              APIKey, Agent, PlanPricing, Company,
                              ReferralKey, ReferrerHistory,
                              DeletedUser, Invoice,
                              ReSeller, ConferenceDetails,
                              PhoneConfMinutePack, UsageHistory)
from surflyapp.forms import UserProfileForm
from surflyapp.invoice import TerminateInvoiceClient


logger = logging.getLogger(__name__)


SIGNUP_REPORT_EMAIL = settings.SIGNUP_REPORT_EMAIL

STRIPE_PUBLISHABLE_KEY = settings.STRIPE_PUBLISHABLE_KEY
stripe.api_key = settings.STRIPE_SECRET_KEY

QUADERNO_API = settings.QUADERNO_API
QUADERNO_TOKEN = settings.QUADERNO_TOKEN

GEOIP = GeoIP.new(GeoIP.GEOIP_MEMORY_CACHE)

SHA1_RE = re.compile('^[a-f0-9]{40}$')


@login_required
@is_company_admin
def profile_api(request):
    """
    User can see their API keys and add/remove them
    """
    company = request.company
    try:
        apikey = APIKey.objects.get(user=request.user, company=company, enabled=True)
        snippet = Template(settings.SNIPPET).substitute(widgetkey=apikey.widget_key, servername=request.get_host())
    except APIKey.DoesNotExist:
        apikey = None
        snippet = ''

    if request.method == 'POST':
        domain_list = request.POST.get('domain_list')
        company.domains = domain_list
        company.save()
        messages.success(request, "Your company domains list updated successfully !")

    snippet = None
    if apikey:
        snippet = Template(settings.SNIPPET).substitute(widgetkey=apikey.widget_key, servername=request.get_host())
    return render(request, 'profile_api.html', {'apikey': apikey,
                                                'SERVER_NAME': request.get_host(),
                                                'snippet': snippet,
                                                'company_domains': company})


@login_required
def profile(request):
    """
    User can manage their profile name.
    """
    profile_form = UserProfileForm(instance=request.user)
    reg_profile = RegistrationProfile.objects.get(user=request.user)
    can_delete_account = True
    context_data = RequestContext(request)
    is_company_admin = context_data.get("is_company_admin")
    company = context_data.get("company")

    if request.method == 'POST':
        profile_form = UserProfileForm(data=request.POST)

        if profile_form.is_valid():
            error_message = ''
            user = request.user
            user.first_name = profile_form.cleaned_data['first_name']
            user.save()

            reg_profile.time_zone = request.POST.get('time_zone')
            reg_profile.save()

            if is_company_admin:
                company_name = request.POST.get("company_name").strip()
                if len(company_name) <= 200:
                    company.name = company_name or settings.DEFAULT_COMPANY_NAME
                    company.save()
                else:
                    error_message = "Company name is too long (Maximum of 200 characters)"

            webhook_error_message = set_reseller_webhook_or_error_message(request)
            if webhook_error_message or error_message:
                messages.error(request, error_message)
                messages.error(request, webhook_error_message)
            else:
                messages.success(request, "Details are updated successfully.")

        else:
            messages.error(request, "Ooops! Some thing went wrong.")

    if is_company_admin and company and company.pricing_plan:
        can_delete_account = False

    is_inactive = not is_user_activated(request.user, request)

    try:
        reseller = ReSeller.objects.get(user__id=request.user.id)
    except ReSeller.DoesNotExist:
        reseller = None

    return render(request, 'profile.html', {'profile_form': profile_form,
                                            'can_delete_account': can_delete_account,
                                            "is_inactive": is_inactive,
                                            "default_company": settings.DEFAULT_COMPANY_NAME,
                                            "time_zone": reg_profile.time_zone,
                                            "reseller": reseller})


def set_reseller_webhook_or_error_message(request):
    try:
        reseller = ReSeller.objects.get(user__id=request.user.id)
    except ReSeller.DoesNotExist:
        return ''
    else:
        url = request.POST.get('webhook', "")
        if url:
            try:
                url_obj = ValidURL(url)
            except InvalidURL:
                return "Enter a valid webhook url"

        reseller.webhook = url_obj.cleaned_url
        reseller.save()
        return ''


def resend_own_activation_mail(request):
    user = request.user
    site = get_site(request)
    activation_key = RegistrationProfile.objects.get(user=user).activation_key

    to_email_list = [user.email]
    activation_url = 'https://{}{}'.format(site.domain, reverse("registration_activate", kwargs={"activation_key": activation_key}))
    ctx_dict = {'activation_url': activation_url,
                'site_name': site.name, 'name': user.first_name}
    subject = render_to_string('registration/activation_email_subject.txt',
                               context_dict)
    # Email subject *must not* contain newlines
    subject = ''.join(subject.splitlines())

    spawn(custom_send_mail, template_name="registration_activation_email",
          ctx_dict=ctx_dict, to=[user.email], subject=subject)

    messages.success(request, "Mail sent successfully.")
    return HttpResponseRedirect(reverse("user_profile"))


@login_required
@is_company_admin
def profile_plan(request):
    """
    User can see their plans
    User can upgrade the plans
    """
    remaining_conf_time = 0
    available_packs = None
    company = request.company
    _ip = get_client_ip(request)
    plan_type = ""
    plan_name = company.pricing_plan.name if company.pricing_plan else 'free'

    try:
        billing_info = BillingInfo.objects.get(company=company)
        has_billing_info = True if billing_info.cc_last_4 and company.stripe_customer_id else False
    except BillingInfo.DoesNotExist:
        has_billing_info = False

    if company.pricing_plan:
        plan_type = "business" if company.pricing_plan.name.__contains__("business") else "pro"
        conf, created = ConferenceDetails.objects.get_or_create(company=company)
        if created:
            conf.free_minutes = (conf.company.pricing_plan.conf_minute * conf.company.agent_size)
            conf.save()

        remaining_conf_time += conf.free_minutes + conf.paid_minutes
        available_packs = PhoneConfMinutePack.objects.filter(available_plans=company.pricing_plan).order_by("price")

    is_invoice_user = is_client_type_of(company, 'advance_invoice,usage_invoice')
    is_trail_user = is_client_type_of(company, 'trial')

    params = {
        'company': company,
        'plan_prices': get_price(),
        "plan_type": plan_type,
        "plan_name": plan_name,
        "has_billing_info": has_billing_info,
        "is_dollar": is_dollar_user(_ip, company),
        "remaining_conf_time": remaining_conf_time,
        "available_packs": available_packs,
        "is_invoice_user": is_invoice_user,
        "is_trail_user": is_trail_user,
    }
    return render(request, 'profile_plan.html', params)


@memoize()
def get_price():
    plan_prices = {}
    stripe_plan = stripe.Plan.all()
    for plan in PlanPricing.objects.exclude(is_active=False):
        stripe_plan_id = plan.name
        plan_prices.update({plan.name: data.amount / 100
                            for data in stripe_plan.data
                            if data.id == stripe_plan_id})
    return plan_prices


@memoize()
def is_dollar_user(ip, company):
    if company.stripe_customer_id:
        try:
            stripe_customer = stripe.Customer.retrieve(company.stripe_customer_id)
            return stripe_customer.currency == "usd"
        except stripe.error.InvalidRequestError:
            pass

    try:
        billing_info = BillingInfo.objects.get(company=company)
        return str(billing_info.country) == "US"
    except BillingInfo.DoesNotExist:
        pass

    try:
        geo_country = GEOIP.country_code_by_addr(ip)
    except TypeError:
        geo_country = "US"
    return geo_country == "US"


@require_POST
@login_required
@is_company_admin
def activate_stripe_plan(request, plan_name=""):
    """
    The company admin user or reseller user can enable or activate the API
    and Plan for the company.
    """
    user = request.user
    company = request.company

    _ip = get_client_ip(request)
    is_usd = is_dollar_user(_ip, company)

    plan_name = request.POST.get("plan", plan_name)
    if plan_name:
        plan_name = plan_name + "_usd" if is_usd else plan_name

    stripe_customer = get_or_create_customer(request)
    if plan_name:
        set_stripe_subscription(request, stripe_customer, plan_name)

        if not is_user_activated(user, request):
            activate_account(user)
        enable_apikey(request)

        _ip = get_client_ip(request)
        ctx_dict = {"info": 'email={} userid={} ip={}'.format(user.email, user.id, _ip),
                    "host": request.get_host(),
                    "email": user.email}

        logger.debug('New paid user: {}'.format(info))

        spawn(custom_send_mail,
              template_name="new_user_subscription",
              ctx_dict=ctx_dict,
              from_email=SIGNUP_REPORT_EMAIL,
              to=[SIGNUP_REPORT_EMAIL],
              subject='%s: New paid user: %s' % (request.get_host(), user.email))

    # call webhook
    reseller = ReSeller.objects.filter(company=company).first()
    if reseller:
        action = "upgrade_subscription"
        log_and_call_custom_action_webhook(reseller, company, action)

    # Update phone conf free minute
    update_free_conf_minute(company)

    response_data = dict()
    response_data["state"] = "ok"
    response_data["plan"] = plan_name
    return HttpResponse(json.dumps(response_data), content_type="application/json")


def cancel_invoice_subscription(user):
    agent = Agent.objects.get(user=user)
    company = agent.company
    if company.pricing_plan:
        invoice_cancel = TerminateInvoiceClient()
        invoice_cancel(company)
        # call webhook
        reseller = ReSeller.objects.filter(company=company).first()
        if reseller:
            action = "downgrade_subscription"
            log_and_call_custom_action_webhook(reseller, company, action)


def get_or_create_customer(request):

    company = request.company
    token = request.POST.get("token[id]")
    try:
        stripe_customer = stripe.Customer.retrieve(company.stripe_customer_id)
        if token:
            billing_info = BillingInfo.objects.get(company=company)
            stripe_customer.card = token
            stripe_customer.save()
    except stripe.error.InvalidRequestError:
        if not token:
            raise Exception("Going to create a stripe customer with None token for company : {}".format(company.id))
        billing_info = BillingInfo.objects.get(company=company)
        customer_metadata = {'user_id': request.user.id,
                             "SERVER_NAME": request.get_host(),
                             "first_name": billing_info.name,
                             "street_line_1": billing_info.address,
                             'city': billing_info.city,
                             "postal_code": billing_info.zip_address,
                             "country": billing_info.country,
                             "tax_id": billing_info.vatid}
        stripe_customer = stripe.Customer.create(email=request.user.email,
                                                 metadata=customer_metadata,
                                                 card=token)
    company.stripe_customer_id = stripe_customer.id
    company.save()
    if token:
        billing_info.cc_last_4 = stripe_customer.cards.data[0].last4
        billing_info.save()
    return stripe_customer


def set_stripe_subscription(request, stripe_customer, plan_name):
    company = request.company
    bill_info = BillingInfo.objects.get(company=company)

    stripe_subscription_id = company.stripe_subscription_id
    _ip = get_client_ip(request)

    def create_subscription():
        tax_json = get_quaderno_tax_rate(bill_info)
        subscription_meta = tax_json
        subscription_meta.update({"ip_address": _ip})
        stripe_subscription = stripe_customer.subscriptions.create(plan=plan_name,
                                                                   quantity=company.agent_size,
                                                                   tax_percent=tax_json["rate"],
                                                                   metadata=subscription_meta)
        QuadernoTransaction.objects.get_or_create(company=company)
        company.stripe_subscription_id = stripe_subscription.id
        company.save()

    if not stripe_subscription_id:
        create_subscription()
    else:
        try:
            stripe_subscription = stripe_customer.subscriptions.retrieve(stripe_subscription_id)
        except stripe.error.InvalidRequestError:
            create_subscription()
        else:
            stripe_subscription.plan = plan_name
            stripe_subscription.quantity = company.agent_size
            stripe_subscription.save()
            stripe_customer.save()

    plan = PlanPricing.objects.get(name=plan_name)
    company.pricing_plan = plan
    company.save()
    return plan_name


@require_POST
@login_required
@is_company_admin
def deactivate_stripe_plan(request):
    """
    The company admin user or reseller user can disable or cancel the API
    and Plan for the company.
    """
    user = request.user
    company = request.company

    _ip = request.environ.get('HTTP_X_FORWARDED_FOR')
    info = 'email=%s userid=%s ip=%s' % (user.email, user.id, _ip)
    ctx_dict = {"info": info,
                "host": request.get_host(),
                "email": user.email}

    response_data = {"state": "FAIL"}

    stripe_customer_id = company.stripe_customer_id
    stripe_subscription_id = company.stripe_subscription_id

    try:
        customer = stripe.Customer.retrieve(stripe_customer_id)
        customer.subscriptions.retrieve(stripe_subscription_id).delete()
    except stripe.error.StripeError as reason:
        logger.debug('FAILED to cancel account: {}, Stripe Error: {}'.format(info, reason))
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    disable_apikey(request)
    company.pricing_plan = None
    company.save()

    spawn(custom_send_mail,
          template_name="subscription_cancelled",
          ctx_dict=ctx_dict,
          from_email=SIGNUP_REPORT_EMAIL,
          to=[SIGNUP_REPORT_EMAIL],
          subject='%s: Account cancelled user: %s' % (request.get_host(), user.email))

    # call webhook
    reseller = ReSeller.objects.filter(company=company).first()
    if reseller:
        action = "downgrade_subscription"
        log_and_call_custom_action_webhook(reseller, company, action)

    logger.debug('Account cancelled: {}'.format(info))
    response_data['state'] = "OK"
    return HttpResponse(json.dumps(response_data), content_type="application/json")


def mailchimp_newsletter_subscribe(sender, user, request, cleaned_data, **kwargs):
    # XXX check if host is surfly.com
    cleaned_data.pop('password', None)
    if cleaned_data['newsletter']:
        SubscribeJob(user.email, '').start()  # XXX
    _ip = request.environ.get('HTTP_X_FORWARDED_FOR')
    info = 'email=%s userid=%s newsletter=%s ip=%s' % (
        user.email, user.id, cleaned_data.get('newsletter'), _ip)
    logger.debug('New registration: {}'.format(info))

    ctx_dict = {"info": info, "email": cleaned_data.get('email')}

    spawn(custom_send_mail,
          template_name="new_registration",
          ctx_dict=ctx_dict,
          from_email=SIGNUP_REPORT_EMAIL,
          to=[SIGNUP_REPORT_EMAIL],
          subject='New registration: %s' % cleaned_data.get('email'))


user_registered.connect(mailchimp_newsletter_subscribe)


def disable_apikey(request):
    try:
        api_key = APIKey.objects.filter(user=request.user, company=request.company)
        api_key.delete()
    except (IntegrityError, APIKey.DoesNotExist):
        return False
    return True


def enable_apikey(request):
    try:
        api_key = APIKey.objects.get(user=request.user, company=request.company)
        api_key.enabled = True
        api_key.save()
    except (IntegrityError, APIKey.DoesNotExist, APIKey.MultipleObjectsReturned):
        return create_apikey(request)
    return True


def create_apikey(request):
    try:
        if request.company.pricing_plan:
            APIKey.objects.create(user=request.user, enabled=True,
                                  company=request.company)
        else:
            return False
    except FieldError:
        return False
    return True


def regenerate_apikey(request, key_name=None):
    try:
        api_key = APIKey.objects.get(user=request.user, company=request.company, enabled=True)
        widget_key = api_key.widget_key
        if key_name == 'api_key':
            # apikey is primary_key
            api_key.delete()
            api_key = APIKey.objects.create(user=request.user, enabled=True,
                                            company=request.company)
            api_key.widget_key = widget_key
            api_key.save()
        if key_name == 'widget_key':
            api_key.widget_key = uuid.uuid4().hex
        api_key.save()
        analytics.track(request.user.username, 'Regenerated {0}'.format(key_name))
        return True
    except APIKey.DoesNotExist:
        return create_apikey(request)


@login_required
@is_company_admin
def generate_api_key(request, *args, **kwargs):
    key_name = kwargs.get('key_name', None)
    if not regenerate_apikey(request, key_name):
        messages.error(request, "Ooops! New api_key cannot be created.")
    return HttpResponseRedirect(reverse("profile_api"))


@login_required
@is_company_admin
def remove_api_key(request):
    if not disable_apikey(request):
        messages.error(request, "Ooops! You do not have any active api_key.")
    return HttpResponseRedirect(reverse("profile_api"))


@login_required
@is_company_admin
def billing_info(request, **kwargs):
    company = request.company
    _ip = get_client_ip(request)
    is_usd = is_dollar_user(_ip, company)
    is_cc_user = company.stripe_customer_id

    plan_name = kwargs.get("plan_name", None)
    plan = company.pricing_plan
    is_invoice_user = is_client_type_of(company, 'advance_invoice,usage_invoice')
    if plan_name:
        plan_name = plan_name + "_usd" if is_usd else plan_name
        plan = PlanPricing.objects.get(name=plan_name)

    if request.method == "POST":
        billing_info, created = BillingInfo.objects.get_or_create(company=company)
        billing_info.name = request.POST.get("billing_name")
        billing_info.address = request.POST.get("billing_street")
        billing_info.zip_address = request.POST.get("billing_zip")
        billing_info.city = request.POST.get("billing_city")
        billing_info.country = request.POST.get("billing_country") or 'NL'
        billing_info.vatid = request.POST.get("vat_id")
        billing_info.bill_email = request.POST.get("bill_email")
        billing_info.ip = get_client_ip(request)
        billing_info.save()
        new_card = request.POST.get("new_card")
        if is_cc_user and new_card == 'false':
            if plan_name:
                return activate_stripe_plan(request, plan_name)
            elif plan:
                update_stripe_metadata(request, company, billing_info)
        # Check Quaderno Conatct
        create_or_update_quaderno_contact(company)
        return HttpResponse(json.dumps({'status': 'OK'}), content_type="application/json")
    else:
        try:
            billing = BillingInfo.objects.get(company=company)
        except BillingInfo.DoesNotExist:
            billing = None

        if plan_name:
            button_message = "Start your plan!"
        elif plan or billing:
            button_message = "Update"
        else:
            messages.warning(request, "Please choose your plan.")
            return HttpResponseRedirect(reverse("profile_plan"))

        try:
            geo_country = GEOIP.country_code_by_addr(_ip)
        except TypeError:
            geo_country = "US"

        currency = "usd" if is_usd else "eur"

        return render(request, 'billing_info.html', {
            'billing': billing, "plan": plan, 'geo_country': geo_country,
            'plan_name': plan_name,
            'button_message': button_message, 'is_cc_user': is_cc_user,
            'STRIPE_PUBLISHABLE_KEY': STRIPE_PUBLISHABLE_KEY, 'currency': currency,
            'is_invoice_user': is_invoice_user, "admin_email": company.admin_email()
        })


@csrf_exempt
def quaderno_change_webhook(request):
    if not request.body:
        return HttpResponse(content='No request data found. Only contact.updated event allowed', status=200)

    event_json = json.loads(request.body)
    if event_json["event_type"] != "contact.updated":
        return HttpResponse(content='Not a valid event. Only contact.updated allowed', status=200)

    contact = event_json["data"]["object"]
    quaderno = QuadernoTransaction.objects.get(contact_id=contact["id"])
    billing = BillingInfo.objects.get(company=quaderno.company)

    # Update Billing address
    billing.name = contact["first_name"]
    billing.address = contact["street_line_1"] + contact["street_line_2"]
    billing.zip_address = contact["postal_code"]
    billing.city = contact["city"]
    billing.country = contact["country"]
    billing.vatid = contact["tax_id"]
    billing.save()
    return HttpResponse(content='Contact for id: {} updated successfully!'.format(contact["id"]), status=200)


def update_stripe_metadata(request, company, bill_info):
    customer_metadata = {'user_id': request.user.id,
                         "SERVER_NAME": request.get_host(),
                         "first_name": bill_info.name,
                         "street_line_1": bill_info.address,
                         'city': bill_info.city,
                         "postal_code": bill_info.zip_address,
                         "country": bill_info.country,
                         "tax_id": bill_info.vatid}
    tax_json = get_quaderno_tax_rate(bill_info)
    subscription_metadata = tax_json
    subscription_metadata.update({"ip_address": bill_info.ip})

    stripe_customer = stripe.Customer.retrieve(company.stripe_customer_id)
    stripe_subscription = stripe_customer.subscriptions.retrieve(company.stripe_subscription_id)
    stripe_customer.metadata = customer_metadata
    stripe_subscription.tax_percent = tax_json["rate"]
    stripe_subscription.metadata = subscription_metadata
    stripe_customer.save()
    stripe_subscription.save()


@require_POST
@login_required
@is_company_admin
def update_card(request):
    get_or_create_customer(request)
    return HttpResponse(json.dumps({'state': "OK"}), content_type="application/json")


@require_POST
@login_required
@is_company_admin
def get_invoice_link(request):
    response_data = dict(status="FAIL")
    company = request.company
    try:
        quaderno_transaction = QuadernoTransaction.objects.get(company=company)
    except QuadernoTransaction.DoesNotExist:
        pass
    else:
        if not quaderno_transaction.permalink:
            permalink = generate_permalink(company)
        else:
            permalink = quaderno_transaction.permalink
        if permalink:
            response_data["permalink"] = permalink
            response_data["status"] = "OK"
    return HttpResponse(json.dumps(response_data), content_type="application/json")


def generate_permalink(company):
    if not company.pricing_plan and not company.stripe_customer_id:
        return ''
    return create_or_update_quaderno_contact(company).get('permalink', '')


@login_required
def share_surfly(request):
    referral_key_obj, create = ReferralKey.objects.get_or_create(user=request.user)

    analytics.identify(request.user.username, {
        'referralkey': referral_key_obj.referral_key
    })

    referrer, referrer_created = ReferrerHistory.objects.get_or_create(user=request.user)
    total_registered_user = 0
    total_paid_user = 0
    total_point = referrer.total_point
    if not referrer_created and total_point:
        total_registered_user = len(referrer.registered_user.all())
        total_paid_user = len(referrer.paid_company.all())

    site = get_site(request)

    if request.method == "POST":
        _emails = request.POST.get("share_email")
        to_email_list = [_email.strip() for _email in _emails.split(",") if _email.strip()]
        subject = "Sign-up to {}".format(site.name)
        referral_url = 'https://{}/register/?ref={}'.format(site.domain, referral_key)
        context_dict = {
            'referral_url': referral_url, "referrer": request.user.first_name
        }
        spawn(custom_send_mail, template_name='share_surfly_mail', ctx_dict=context_dict, to=to_email_list)
        messages.success(request, "You have successfully shared {}!".format(site.name))

    agent = Agent.objects.get(user=request.user)
    remaining_session_point = int(agent.session_quota_pack) + int(agent.session_quota_monthly)

    return render(request, "share_surfly.html", {'referral_key': referral_key_obj.referral_key,
                                                 "total_registered_user": total_registered_user,
                                                 'total_paid_user': total_paid_user,
                                                 "total_point": total_point,
                                                 "remaining_session_point": remaining_session_point,
                                                 'site': site,
                                                 'REFERRAL_POINTS_FREE_PLAN': settings.REFERRAL_POINTS_FREE_PLAN,
                                                 "REFERRAL_POINTS_PAID_PLAN": settings.REFERRAL_POINTS_PAID_PLAN})


@require_POST
@csrf_exempt
def successful_charge_webhook(request):
    event_json = json.loads(request.body)
    if event_json["type"] != "charge.succeeded":
        return HttpResponse(status=404)
    charged_customer = event_json['data']["object"]["customer"]
    try:
        company = Company.objects.get(stripe_customer_id=charged_customer)
    except Company.DoesNotExist:
        logger.debug("Stripe Webhook ERROR: Unable to find company for: %s" % event_json)
        raise Company.DoesNotExist("for stripe customer = {}".format(charged_customer))

    referrer_user_list = ReferrerHistory.objects.filter(registered_company=company)
    if referrer_user_list:
        referrer_user = referrer_user_list[0]
        agent = Agent.objects.get(user=referrer_user.user)
        agent.session_quota_pack += settings.REFERRAL_POINTS_PAID_PLAN
        agent.save()

        referrer_user.paid_company.add(company)
        referrer_user.total_point += settings.REFERRAL_POINTS_PAID_PLAN
        referrer_user.save()
    return HttpResponse(status=200)


@require_POST
@login_required
def delete_account(request):
    response_data = {"state": "FAIL"}
    user = request.user
    context_data = RequestContext(request, True)
    if context_data["is_reseller"]:
        response_data["message"] = "You are a reseller. To delete account, please contact support!"
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    agent = Agent.objects.get(user__id=user.id)
    if not can_change_admin(agent):
        response_data["message"] = "You are the only admin, so this action can't be performed !"
        return HttpResponse(json.dumps(response_data), content_type="application/json")

    monthly_quota = agent.session_quota_monthly if agent.session_quota_monthly else 0

    old_user, created = DeletedUser.objects.get_or_create(encrypt_email=hash_email(user.email))
    old_user.rest_quota = (agent.session_quota_pack + monthly_quota)
    old_user.save()

    if agent.is_company_admin:
        company = Company.objects.get(id=agent.company.id)
        non_admin_agents = Agent.objects.filter(company=company, is_company_admin=False)
        for _non_admin in non_admin_agents:
            _non_admin.user.delete()
        company.delete()
    User.objects.get(id=user.id).delete()
    response_data["state"] = "OK"
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@login_required
@is_company_admin
def display_invoice_list(request, **kwargs):
    if settings.DEBUG:
        site = get_site(request)
    else:
        site = settings.AWS_S3_CUSTOM_DOMAIN
    invoice_list = []
    year_list = []
    invoice_model_obj = Invoice.objects.filter(company=request.company)

    try:
        year = int(kwargs['year'])
    except (KeyError, ValueError):
        year = int(date.today().year)

    invoice_list = invoice_model_obj.filter(subscription_enddate__year=year)

    max_date = invoice_model_obj.aggregate(Max('subscription_enddate')).get("subscription_enddate__max")
    min_date = invoice_model_obj.aggregate(Min('subscription_enddate')).get("subscription_enddate__min")
    if max_date and min_date:
        # To avoid NoneType/AttributeError
        max_year = max_date.year
        min_year = min_date.year
        year_list = xrange(min_year, max_year + 1)

    return render(request, 'invoice/invoice_list.html', {
        'invoice_list': invoice_list,
        'year_list': year_list,
        'current_year': year,
        'site': site})


def activate_meeting_user(request, *args, **kwargs):
    # Make sure the key we're trying conforms to the pattern of a
    # SHA1 hash; if it doesn't, no point trying to look it up in
    # the database.
    _activated = u"ALREADY_ACTIVATED"
    activation_key = kwargs['activation_key']

    message_head = 'Activationkey Error'
    message = 'Your activation link is error.Try again later'

    if SHA1_RE.search(activation_key):
        try:
            profile = RegistrationProfile.objects.get(
                activation_key=activation_key)

            if not profile.activation_key_expired():
                user = profile.user
                user.is_active = True
                user.save()

                agent = Agent.objects.get(user=user)
                agent.session_quota_monthly += (settings.MAX_SESSIONS_FREE_PLAN - 1)
                agent.save()

                profile.activation_key = _activated
                profile.time_zone = get_time_zone_by_ip(request)
                profile.save()

                return render(request,
                              'registration/meeting_new_user_password.html',
                              {'user_id': user.id})

        except RegistrationProfile.DoesNotExist:
            message_head = 'Invalid Activationkey'
            message = 'You have already activated your account.'

    return render(request, 'message.html',
                  {
                      'message_head': message_head,
                      'message': message
                  })


@login_required
@is_company_admin
def conference(request):
    conf = None
    company = request.company

    if request.POST:
        selected_pack_id = request.POST.get("pack_id")
        try:
            minutes_pack = PhoneConfMinutePack.objects.get(id=selected_pack_id)
        except PhoneConfMinutePack.DoesNotExist:
            messages.error(request, "Oops, Invalid Pack. Please select correct pack!")
        else:
            conf = ConferenceDetails.objects.get(company=company)
            upgrade_response = charge_extra_minutes_pack(minutes_pack, company, conf)
            if upgrade_response:
                upgrade_minutes_pack(minutes_pack, conf)
                messages.success(request, "Successfully upgraded!")
            else:
                messages.error(request, "Something went wrong while trying to charge your CC!")

    return HttpResponse(json.dumps({"status": "OK"}), content_type="application/json")


def charge_extra_minutes_pack(pack, company, conf_details=None):
    try:
        charge = stripe.Charge.create(customer=company.stripe_customer_id,
                                      amount=int(pack.price * 100),
                                      currency=pack.currency,
                                      metadata={"company_name": company.name,
                                                "pack": pack.name})
    except stripe.error.InvalidRequestError:
        import traceback
        traceback.print_exc()
        return False
    else:
        def update_charge_to_db(company, charge, conf_details):
            if not conf_details:
                conf_details = ConferenceDetails.objects.get(company=company)
            if conf_details.stripe_charges:
                conf_details.stripe_charges += "{},".format(charge.id)
            else:
                conf_details.stripe_charges = "{},".format(charge.id)
            conf_details.save()
        spawn(update_charge_to_db, company, charge, conf_details)
        return True


def upgrade_minutes_pack(pack, conf_details):
    conf_details.paid_minutes += pack.minutes
    conf_details.total_paid_minutes_bought += pack.minutes
    conf_details.save()


@require_POST
@csrf_exempt
def conference_update_minutes(request):
    usage = UsageHistory.objects.get(session_id=request.POST.get("session_id"))
    update_conf_minutes(usage)
    response_data = {"state": "OK"}
    return HttpResponse(json.dumps(response_data), content_type="application/json")
