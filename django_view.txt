def addebay(request, **kwargs):
    product = Product.objects.get(id=kwargs.get('product_id'))
    # specify the connection to the eBay environment
    connection = httplib.HTTPSConnection(serverUrl)
    # specify a POST with the results of generateHeaders and generateRequest
    # detailLevel = 1, ViewAllNodes = 1  - this gets the entire tree
    verb = "AddItem"
    connection.request("POST", serverDir, buildRequestXml("ReturnAll", "1", product), buildHttpHeaders(verb))
    response = connection.getresponse()
    if response.status != 200:
        print "Error sending request: " + response.reason
        exit
    else: #response successful
        # store the response data and close the connection
        data = response.read()
        connection.close()

        # parse the response data into a DOM
        response = parseString(data)
        # check for any Errors
        errorNodes = response.getElementsByTagName('Errors')

        if (errorNodes != []): #there are errors
            logger.info("eBay returned the following errors")
            #Go through each error:
            for error in errorNodes:
                #output the error code and short message
                logger.info("{} : {}".format(((error.getElementsByTagName('ErrorCode')[0]).childNodes[0]).nodeValue, 
                ((error.getElementsByTagName('ShortMessage')[0]).childNodes[0]).nodeValue.replace("<", "&lt;"))

                messages.add_message(request, ERROR, ((error.getElementsByTagName('ErrorCode')[0]).childNodes[0]).nodeValue)
                messages.add_message(request, ERROR, ((error.getElementsByTagName('ShortMessage')[0]).childNodes[0]).nodeValue)
                    #output Long Message if it exists (depends on ErrorLevel setting)
                if (error.getElementsByTagName('LongMessage')!= []):
                    logger.info("{}".format(((error.getElementsByTagName('LongMessage')[0]).childNodes[0]).nodeValue.replace("<", "&lt;"))
                    messages.add_message(request, ERROR, ((error.getElementsByTagName('LongMessage')[0]).childNodes[0]).nodeValue)

            # check for the <ItemID> with warning message
            if (response.getElementsByTagName('ItemID')!=[]):
                logger.info("Item ID is: {}".format(((response.getElementsByTagName('ItemID')[0]).childNodes[0]).nodeValue)
                product.ebay_item_id = ((response.getElementsByTagName('ItemID')[0]).childNodes[0]).nodeValue
                product.eBay_API_called = True
                product.save()
                messages.add_message(request, SUCCESS, 'eBay ItemID added in database')
        else: #eBay returned no errors - output results
            # check for the <ItemID> tag and print
            if (response.getElementsByTagName('ItemID')!=[]):
                logger.info("Item ID is: {}".format(((response.getElementsByTagName('ItemID')[0]).childNodes[0]).nodeValue)
                product.ebay_item_id = ((response.getElementsByTagName('ItemID')[0]).childNodes[0]).nodeValue
                product.eBay_API_called = True
                product.save()
                messages.add_message(request, SUCCESS, 'Item created in API')

        response.unlink()
        return HttpResponseRedirect('/admin/products/product/')
